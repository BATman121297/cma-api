const {
  portSelectAlls,
  serialDataReads
} = require("../../src/controller/com-port/app");

const port = ({ router, makeExpressCallback, verifyTokens }) => {
  //########################
  // POST REQUESTS
  //########################

  // receive data from serial port
  router.post("/receive", verifyTokens, makeExpressCallback(serialDataReads));

  //########################
  // END POST REQUESTS
  //########################

  //########################
  // PUT REQUESTS
  //########################

  //########################
  // END PUT REQUESTS
  //########################

  //########################
  // GET REQUESTS
  //########################

  // select all actions
  router.get("/select", verifyTokens, makeExpressCallback(portSelectAlls));

  //########################
  // END GET REQUESTS
  //########################

  return router;
};

module.exports = port;
