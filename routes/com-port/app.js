const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");

const port = require("./routes");

const { verifyTokens } = require("../../src/token/app");
//#########
const ports = port({ router, makeExpressCallback, verifyTokens });

const services = Object.freeze({
  ports
});

module.exports = services;

module.exports = {
  ports
};

module.exports = router;
