const {
  rolesAddNews,
  rolesUpdates,
  selectAllroles,
  selectOneRoles
} = require("../../src/controller/roles/app");

const role = ({ router, makeExpressCallback, verifyTokens }) => {
  //########################
  // POST REQUESTS
  //########################

  //   add new access rights
  router.post("/add", verifyTokens, makeExpressCallback(rolesAddNews));

  //########################
  // END POST REQUESTS
  //########################

  //########################
  // PUT REQUESTS
  //########################

  // update existing roles
  router.put("/update/:id", verifyTokens, makeExpressCallback(rolesUpdates));

  //########################
  // END PUT REQUESTS
  //########################

  //########################
  // GET REQUESTS
  //########################

  // select all roles
  router.get("/select", verifyTokens, makeExpressCallback(selectAllroles));

  // select single role
  router.get("/select/:id", verifyTokens, makeExpressCallback(selectOneRoles));

  //########################
  // END GET REQUESTS
  //########################

  return router;
};

module.exports = role;
