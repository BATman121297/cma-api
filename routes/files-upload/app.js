const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");

const upload = require("./routes");

const { uploadFiless } = require("../../src/middlewares/app");

const { verifyTokens } = require("../../src/token/app");

//#########
const uploads = upload({
  router,
  makeExpressCallback,
  uploadFiless,
  verifyTokens
});

const services = Object.freeze({
  uploads
});

module.exports = services;

module.exports = {
  uploads
};

module.exports = router;
