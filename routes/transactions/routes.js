const {
  transactionAddNews,
  selectAllTransactionss,
  c_addInbound,
  c_addOutbound,
  purchaseOrderFetchs,
  notifForTransmittals,
  c_selectAllWarehouses,
  c_scanTransaction,
  grpoFsqrs,
  transactionUpdates
} = require("../../src/controller/transactions/app");

const transaction = ({
  router,
  makeExpressCallback,
  verifyTokens,
  fsqrValidates
}) => {
  // POST

  // add
  router.post("/add", verifyTokens, makeExpressCallback(transactionAddNews));

  router.post("/scan", verifyTokens, makeExpressCallback(c_scanTransaction));

  // GET

  router.get(
    "/selectAllWarehouses",
    verifyTokens,
    makeExpressCallback(c_selectAllWarehouses)
  );

  // select all and single transaction if has query code in URL
  router.post(
    "/select",
    verifyTokens,
    makeExpressCallback(selectAllTransactionss)
  );

  router.post("/inbound/add", verifyTokens, makeExpressCallback(c_addInbound));
  router.post(
    "/outbound/add",
    verifyTokens,
    makeExpressCallback(c_addOutbound)
  );
  router.post("/fetch-sap", makeExpressCallback(purchaseOrderFetchs));

  router.get(
    "/notif/for-transmittal",
    verifyTokens,
    makeExpressCallback(notifForTransmittals)
  );

  // validate for fsqr only
  router.get("/grpo/:code", fsqrValidates, makeExpressCallback(grpoFsqrs));

  // update status and attached PO; in FSQR app
  router.post(
    "/update/:code",
    fsqrValidates,
    makeExpressCallback(transactionUpdates)
  );

  return router;
};

module.exports = transaction;
