const {
  usersAddNews,
  selectAllUserss,
  selectOneUsers,
  usersLogins,
  c_updateUser,
  passwordResets,
  selectAllActivityLogs,
  c_employeesInfoSelectAll,
  sapUsersLogins,
  c_checkPin,
  codeAndPwResets
} = require("../../src/controller/users/app");

const user = ({ router, makeExpressCallback, verifyTokens }) => {
  //########################
  // POST REQUESTS
  //########################

  // reset password and code
  router.post("/reset/:id", verifyTokens, makeExpressCallback(codeAndPwResets));

  router.post("/add", verifyTokens, makeExpressCallback(usersAddNews));

  router.post("/check-pin", verifyTokens, makeExpressCallback(c_checkPin));

  // login sap credentials
  router.post("/sap/login", verifyTokens, makeExpressCallback(sapUsersLogins));

  //########################
  // END POST REQUESTS
  //########################

  //########################
  // PUT REQUESTS
  //########################

  // login user to system
  router.put("/login", makeExpressCallback(usersLogins));

  // reset password
  router.put(
    "/reset-password/:id",
    verifyTokens,
    makeExpressCallback(passwordResets)
  );

  //########################
  // END PUT REQUESTS
  //########################

  //########################
  // GET REQUESTS
  //########################

  // select all users
  router.get("/select", verifyTokens, makeExpressCallback(selectAllUserss));

  // select single user
  router.get("/select/:id", verifyTokens, makeExpressCallback(selectOneUsers));

  // select all activity logs
  router.post(
    "/select-logs",
    verifyTokens,
    makeExpressCallback(selectAllActivityLogs)
  );

  //########################
  // END GET REQUESTS
  //########################

  router.put("/update/:id", verifyTokens, makeExpressCallback(c_updateUser));

  router.get(
    "/select-employeeInfos",
    verifyTokens,
    makeExpressCallback(c_employeesInfoSelectAll)
  );

  return router;
};

module.exports = user;
