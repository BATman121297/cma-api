const {
  transactionTypeAddNews,
  selectAllTransactionTypes,
  selectOneTransactionTypes,
  transactionTypeUpdates
} = require("../../src/controller/transaction-type/app");

const transationType = ({ router, makeExpressCallback, verifyTokens }) => {
  // POST

  // add new
  router.post(
    "/add",
    verifyTokens,
    makeExpressCallback(transactionTypeAddNews)
  );

  // GET

  // select all
  router.get(
    "/select",
    verifyTokens,
    makeExpressCallback(selectAllTransactionTypes)
  );

  // select one
  router.get(
    "/select/:id",
    verifyTokens,
    makeExpressCallback(selectOneTransactionTypes)
  );

  // PUT

  // update
  router.put(
    "/update/:id",
    verifyTokens,
    makeExpressCallback(transactionTypeUpdates)
  );

  return router;
};

module.exports = transationType;
