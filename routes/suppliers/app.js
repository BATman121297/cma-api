const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");
const { verifyTokens } = require("../../src/token/app");

const supplier = require("./routes");

const suppliers = supplier({ router, makeExpressCallback, verifyTokens });

const services = Object.freeze({
  suppliers
});

module.exports = services;

module.exports = {
  suppliers
};

module.exports = router;
