const express = require("express");
const router = express.Router();
const makeExpressCallback = require("../../src/express-callback/app");

const weight = require("./routes");

const { verifyTokens } = require("../../src/token/app");
//#########
const weights = weight({ router, makeExpressCallback, verifyTokens });

const services = Object.freeze({
  weights
});

module.exports = services;

module.exports = {
  weights
};

module.exports = router;
