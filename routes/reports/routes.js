const {
  selectAllCompletedTransactions
} = require("../../src/controller/reports/app");

const reports = ({ router, makeExpressCallback, verifyTokens }) => {
  router.post(
    "/select",
    verifyTokens,
    makeExpressCallback(selectAllCompletedTransactions)
  );

  return router;
};

module.exports = reports;
