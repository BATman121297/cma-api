const {
  addNewDrivers,
  updateDrivers,
  driversSelectAlls,
  driversSelectOnes
} = require("../../use-cases/drivers/app");

//####################
const driversAddNew = require("./drivers-add");
const driversUpdate = require("./drivers-update");
const selectAllDriver = require("./drivers-select-all");
const selectOneDriver = require("./drivers-select-one");

//####################
const driversAddNews = driversAddNew({ addNewDrivers });
const driversUpdates = driversUpdate({ updateDrivers });
const selectAllDrivers = selectAllDriver({ driversSelectAlls });
const selectOneDrivers = selectOneDriver({ driversSelectOnes });

const services = Object.freeze({
  driversAddNews,
  driversUpdates,
  selectAllDrivers,
  selectOneDrivers
});

module.exports = services;
module.exports = {
  driversAddNews,
  driversUpdates,
  selectAllDrivers,
  selectOneDrivers
};
