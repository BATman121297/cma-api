const { imageUploads } = require("../../use-cases/file-uploads/app");
const filesUpload = require("./uploads");

const filesUploads = filesUpload({ imageUploads });

const services = Object.freeze({
  filesUploads
});

module.exports = services;
module.exports = {
  filesUploads
};
