const {
  addNewTransactions,
  transactionSelectAlls,
  uc_addInbound,
  uc_addOutbound,
  fetchPurchaseOrders,
  forTransmittalNotifs,
  uc_selectAllWarehouses,
  uc_scanTransaction,
  fsqrGrpos,
  updateTransactions
} = require("../../use-cases/transactions/app");

//####################
const transactionAddNew = require("./transactions-add");
const selectAllTransactions = require("./transactions-select-all");
const addInbound = require("./transactions-inbound-add");
const addOutbound = require("./transactions-outbound-add");
const purchaseOrderFetch = require("./transaction-fetch-SAP");
const notifForTransmittal = require("./notification-for-transmittal");
const selectAllWarehouses = require("./select-all-warehouses");
const scanTransaction = require("./transactions-scan");
const grpoFsqr = require("./transactions-fsqr-grpo");
const transactionUpdate = require("./transactions-update");
//####################

const transactionAddNews = transactionAddNew({ addNewTransactions });
const selectAllTransactionss = selectAllTransactions({ transactionSelectAlls });
const c_addInbound = addInbound({ uc_addInbound });
const c_addOutbound = addOutbound({ uc_addOutbound });
const purchaseOrderFetchs = purchaseOrderFetch({ fetchPurchaseOrders });
const notifForTransmittals = notifForTransmittal({ forTransmittalNotifs });
const c_selectAllWarehouses = selectAllWarehouses({ uc_selectAllWarehouses });
const c_scanTransaction = scanTransaction({ uc_scanTransaction });
const grpoFsqrs = grpoFsqr({ fsqrGrpos });
const transactionUpdates = transactionUpdate({ updateTransactions });

//####################
const services = Object.freeze({
  transactionAddNews,
  selectAllTransactionss,
  c_addInbound,
  c_addOutbound,
  purchaseOrderFetchs,
  notifForTransmittals,
  c_selectAllWarehouses,
  c_scanTransaction,
  grpoFsqrs,
  transactionUpdates
});

module.exports = services;
module.exports = {
  transactionAddNews,
  selectAllTransactionss,
  c_addInbound,
  c_addOutbound,
  purchaseOrderFetchs,
  notifForTransmittals,
  c_selectAllWarehouses,
  c_scanTransaction,
  grpoFsqrs,
  transactionUpdates
};
