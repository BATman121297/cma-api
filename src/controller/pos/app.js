const {
    uc_selectAllPos
  } = require("../../use-cases/pos/app");
  


  const selectAllPos = require("./select-all-pos")

  
  const c_selectAllPos = selectAllPos({ uc_selectAllPos });

  
  const services = Object.freeze({
    c_selectAllPos
  });
  
  module.exports = services;
  module.exports = {
    c_selectAllPos
  };
  