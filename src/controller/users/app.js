const {
  addNewUsers,
  usersSelectAlls,
  usersSelectOnes,
  loginUsers,
  uc_updateUser,
  resetPasswords,
  selectActivityLogss,
  uc_employeesInfoSelectAll,
  loginSapUsers,
  uc_checkPin,
  resetCodeAndPws
} = require("../../use-cases/users/app");

// ##########################

const usersAddNew = require("./users-add");
const selectAllUsers = require("./users-select-all");
const selectOneUser = require("./users-select-one");
const usersLogin = require("./users-login");
const updateUser = require("./users-update");
const passwordReset = require("./users-reset-password");
const selectAllActivityLog = require("./activity-logs-select");
const employeesInfoSelectAll = require("./employeesInfo-select-all");
const sapUsersLogin = require("./users-sap-login");
const checkPin = require("./check-pin");
const codeAndPwReset = require("./reset-password-code");

// ##########################

const c_checkPin = checkPin({ uc_checkPin });
const usersAddNews = usersAddNew({ addNewUsers });
const selectAllUserss = selectAllUsers({ usersSelectAlls });
const selectOneUsers = selectOneUser({ usersSelectOnes });
const usersLogins = usersLogin({ loginUsers });
const c_updateUser = updateUser({ uc_updateUser });
const passwordResets = passwordReset({ resetPasswords });
const selectAllActivityLogs = selectAllActivityLog({ selectActivityLogss });

const c_employeesInfoSelectAll = employeesInfoSelectAll({
  uc_employeesInfoSelectAll
});
const sapUsersLogins = sapUsersLogin({ loginSapUsers });
const codeAndPwResets = codeAndPwReset({ resetCodeAndPws });

// ##############
const services = Object.freeze({
  usersAddNews,
  selectAllUserss,
  selectOneUsers,
  usersLogins,
  c_updateUser,
  passwordResets,
  selectAllActivityLogs,
  c_employeesInfoSelectAll,
  sapUsersLogins,
  c_checkPin,
  codeAndPwResets
});

module.exports = services;
module.exports = {
  usersAddNews,
  selectAllUserss,
  selectOneUsers,
  usersLogins,
  c_updateUser,
  passwordResets,
  selectAllActivityLogs,
  c_employeesInfoSelectAll,
  sapUsersLogins,
  c_checkPin,
  codeAndPwResets
};
