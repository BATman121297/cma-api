const {
  addNewRawMaterials,
  rawMaterialSelectAlls,
  rawMaterialSelectOnes,
  updateRawMaterials
} = require("../../use-cases/raw-material/app");

//####################
const rawMaterialAddNew = require("./raw-material-add");
const selectAllRawMaterial = require("./raw-material-select-all");
const selectOneRawMaterial = require("./raw-material-select-one");
const rawMaterialUpdate = require("./raw-material-update");
//####################
const rawMaterialAddNews = rawMaterialAddNew({ addNewRawMaterials });
const selectAllRawMaterials = selectAllRawMaterial({ rawMaterialSelectAlls });
const selectOneRawMaterials = selectOneRawMaterial({ rawMaterialSelectOnes });
const rawMaterialUpdates = rawMaterialUpdate({ updateRawMaterials });

const services = Object.freeze({
  rawMaterialAddNews,
  selectAllRawMaterials,
  selectOneRawMaterials,
  rawMaterialUpdates
});

module.exports = services;
module.exports = {
  rawMaterialAddNews,
  selectAllRawMaterials,
  selectOneRawMaterials,
  rawMaterialUpdates
};
