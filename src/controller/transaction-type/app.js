const {
  addNewTransactionTypes,
  transactionTypeSelectAlls,
  transactionTypeSelectOnes,
  updateTransactionTypes
} = require("../../use-cases/transaction-type/app");

//####################
const transactionTypeAddNew = require("./transaction-type-add");
const selectAllTransactionType = require("./transaction-type-select-all");
const selectOneTransactionType = require("./transaction-type-select-one");
const transactionTypeUpdate = require("./transaction-type-update");
//####################
const transactionTypeAddNews = transactionTypeAddNew({
  addNewTransactionTypes
});
const selectAllTransactionTypes = selectAllTransactionType({
  transactionTypeSelectAlls
});
const selectOneTransactionTypes = selectOneTransactionType({
  transactionTypeSelectOnes
});
const transactionTypeUpdates = transactionTypeUpdate({
  updateTransactionTypes
});

const services = Object.freeze({
  transactionTypeAddNews,
  selectAllTransactionTypes,
  selectOneTransactionTypes,
  transactionTypeUpdates
});

module.exports = services;
module.exports = {
  transactionTypeAddNews,
  selectAllTransactionTypes,
  selectOneTransactionTypes,
  transactionTypeUpdates
};
