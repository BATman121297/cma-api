const {
  addNewSuppliers,
  suppliersSelectAlls,
  suppliersSelectOnes,
  updateSupplierss
} = require("../../use-cases/suppliers/app");

//####################
const supplierAddNew = require("./supplier-add");
const selectAllSupplier = require("./supplier-select-all");
const selectOneSupplier = require("./supplier-select-one");
const suppliersUpdate = require("./supplier-update");
//####################
const supplierAddNews = supplierAddNew({ addNewSuppliers });
const selectAllSuppliers = selectAllSupplier({ suppliersSelectAlls });
const selectOneSuppliers = selectOneSupplier({ suppliersSelectOnes });
const suppliersUpdates = suppliersUpdate({ updateSupplierss });

const services = Object.freeze({
  supplierAddNews,
  selectAllSuppliers,
  selectOneSuppliers,
  suppliersUpdates
});

module.exports = services;
module.exports = {
  supplierAddNews,
  selectAllSuppliers,
  selectOneSuppliers,
  suppliersUpdates
};
