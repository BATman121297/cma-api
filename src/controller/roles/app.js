const {
  addNewRoless,
  updateRoless,
  rolesSelectAlls,
  rolesSelectOnes
} = require("../../use-cases/roles/app");



//####################

const rolesAddNew = require("./roles-add");
const rolesUpdate = require("./roles-update");
const selectAllrole = require("./roles-select-all");
const selectOneRole = require("./roles-select-one");

//####################
const rolesAddNews = rolesAddNew({ addNewRoless });
const rolesUpdates = rolesUpdate({ updateRoless });
const selectAllroles = selectAllrole({ rolesSelectAlls });
const selectOneRoles = selectOneRole({ rolesSelectOnes });

const services = Object.freeze({
  rolesAddNews,
  rolesUpdates,
  selectAllroles,
  selectOneRoles
});

module.exports = services;
module.exports = {
  rolesAddNews,
  rolesUpdates,
  selectAllroles,
  selectOneRoles
};
