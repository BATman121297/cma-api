const {
    uc_addTruckType,
    uc_selectAllTruckTypes,
    uc_selectOneTruckType,
    uc_updateTruckType
  } = require("../../use-cases/truck-types/app");
  

  const addTruckType = require("./truck-types-add");
  const selectAllTruckTypes = require("./truck-types-select-all")
  const selectOneTruckType = require("./truck-types-select-one")
  const updateTruckType = require("./truck-types-update")
  
  const c_addTruckType = addTruckType({ uc_addTruckType });
  const c_selectAllTruckTypes = selectAllTruckTypes({ uc_selectAllTruckTypes })
  const c_selectOneTruckType = selectOneTruckType({ uc_selectOneTruckType })
  const c_updateTruckType = updateTruckType({ uc_updateTruckType })
  
  
  const services = Object.freeze({
    c_addTruckType,
    c_selectAllTruckTypes,
    c_selectOneTruckType,
    c_updateTruckType
  });
  
  module.exports = services;
  module.exports = {
    c_addTruckType,
    c_selectAllTruckTypes,
    c_selectOneTruckType,
    c_updateTruckType
  };
  