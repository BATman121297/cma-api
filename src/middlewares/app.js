// for image download
const multer = require("multer");
const uniqueString = require("unique-string");
const fs = require("fs");
const path = require("path");

// #############################

const uploadFiles = require("./file-uploader");
const fsqrValidate = require("./fsqr-credentials");
// #############################
const uploadFiless = uploadFiles({ multer, path, uniqueString, fs });
const fsqrValidates = fsqrValidate({});

const services = Object.freeze({
  uploadFiless,
  fsqrValidates
});

module.exports = services;
module.exports = {
  uploadFiless,
  fsqrValidates
};
