const uploadFiles = ({ multer, path, uniqueString, fs }) => {
  return async function uploads() {
    // storage and file name
    const storage = multer.diskStorage({
      destination: (req, file, cb) => {
        cb(null, path.resolve("uploads"));
      },
      filename: (req, file, cb) => {
        cb(null, uniqueString() + "-" + file.originalname.toLowerCase());
      }
    });

    // filter files to upload
    const fileFilter = (req, file, cb) => {
      // .jpg,.jpeg,.png files too
      if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
        cb(null, true);
      } else {
        cb(null, false);
      }
    };

    const paths = path.resolve("uploads");
    // if directory doesn't exits.. make it..
    await fs.access(paths, fs.F_OK, err => {
      if (err) {
        // file doesn't exist
        // so make the uploads folder
        fs.mkdir(paths, err => {
          if (err) {
            console.log(err.message);
          } else {
            console.log("Make the folder..");
          }
        });
        return;
      }
    });

    // to upload image
    const upload = multer({
      storage: storage,
      limits: { fileSize: 1024 * 1024 * 10 },
      fileFilter: fileFilter
    });

    return upload;
  };
};

module.exports = uploadFiles;
