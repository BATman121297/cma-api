const validateAccessRight = () => {
  return async function def(modulesArray, modules, action) {
    let flag = false;

    for (let i = 0; i < modulesArray.length; i++) {
      if (
        modulesArray[i].modulename.toLowerCase() === modules.toLowerCase() &&
        modulesArray[i].modulestatus.toLowerCase() === "active".toLowerCase()
      ) {
        for (let i2 = 0; i2 < modulesArray[i].actions.length; i2++) {
          if (
            modulesArray[i].actions[i2].actionname.toLowerCase() ===
              action.toLowerCase() &&
            modulesArray[i].actions[i2].actionstatus.toLowerCase() ===
              "active".toLowerCase()
          ) {
            flag = true;
          }
        }
      }
    }

    if (!flag) {
      return false;
    } else {
      return true;
    }
  };
};

module.exports = validateAccessRight;
