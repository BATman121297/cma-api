const express = require("express");
const dotenv = require("dotenv");
const cors = require("cors");
const methodOverride = require("method-override");
// const { db } = require("./db/app");
dotenv.config();
const app = express();
const session = require("express-session");
// delays
const delay = s => {
  return new Promise(resolve => setTimeout(resolve, s));
};

// websocket communication
const { socketConnect } = require("./websockets/app");

// select which db to connect
const database = async () => {
  try {
    // determine db before login
    //process.env.PGDATABASE = await db();
    await delay(500);

    // can override requests
    app.use(methodOverride("X-HTTP-Method-Override"));

    // accessible to any
    app.use(cors());

    // Body Parser middleware to handle raw JSON files
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));

    app.use("/uploads", express.static("uploads"));

    const PORT = process.env.PORT || 3000;

    const server = app.listen(PORT, () => {
      console.log(`Server is listening on port ${PORT}.`);
    });

    const io = require("socket.io")(server); //Bind socket.io to our express server.

    module.exports = { io };

    // server should be export before the routes
    // needed for socket.io; for serial communication

    app.use("/drivers", require("../routes/drivers/app"));

    app.use("/truck-scale", require("../routes/truckscale/app"));

    app.use("/modules", require("../routes/modules/app"));

    app.use("/actions", require("../routes/actions/app"));

    app.use("/access-rights", require("../routes/access_rights/app"));

    app.use("/roles", require("../routes/roles/app"));

    app.use("/users", require("../routes/users/app"));

    app.use("/ports", require("../routes/com-port/app"));

    app.use("/seeders", require("../routes/seeders/app"));

    app.use("/files", require("../routes/files-upload/app"));

    app.use("/suppliers", require("../routes/suppliers/app"));

    app.use("/raw-material", require("../routes/raw-material/app"));

    app.use("/truck-types", require("../routes/truck-types/app"));

    app.use("/truck-infos", require("../routes/truck-infos/app"));

    app.use("/transaction-type", require("../routes/transaction-type/app"));

    app.use("/weight-type", require("../routes/weight-types/app"));

    app.use("/transactions", require("../routes/transactions/app"));

    app.use("/reports", require("../routes/reports/app"));

    app.use("/pos", require("../routes/pos/app"));

    module.exports = app;

    // for websocket
    // read data from websocket
    // await socketConnect({ io });
  } catch (e) {
    console.log(e);
  }
};

database();
// end select db
