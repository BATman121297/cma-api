const verifyToken = ({ jwt, userDb }) => {
  return async function token(req, res, next) {
    const { mode } = req.body;
        
    if (!mode) {
      res.sendStatus(403);
      return;
    }

    // only apply JWT on offline mode
    if (mode == 0) {
      const bearerHeader = req.headers["authorization"];
      if (typeof bearerHeader !== "undefined") {
        const bearer = bearerHeader.split(" ");
        const bearerToken = bearer[1];
        req.token = bearerToken;

        const userData = await getUserDataThruToken(req.token, jwt);

        if (userData) {
          const queryToken = await userDb.getTokenOfUser({
            employee_id: userData.user.id,
            password: userData.user.password
          });

          // if token doesn't exist
          if (queryToken.rows.length === 0) {
            res.sendStatus(403);
            return;
          }

          const dbToken = queryToken.rows[0].token;

          if (dbToken === req.token) {
            jwt.verify(
              req.token,
              process.env.ENCRYPTION_KEY,
              (err, authData) => {
                if (err) {
                  res.sendStatus(403);
                  return;
                } else {
                  next();
                }
              }
            );
          } else {
            res.sendStatus(403);
            return;
          }
        } else {
          res.sendStatus(403);
          return;
        }
      } else {
        res.sendStatus(403);
        return;
      }
    } else {
      next();
    }
  };
};

const getUserDataThruToken = async (token, jwt) => {
  let authData = "";
  await jwt.verify(token, process.env.ENCRYPTION_KEY, (err, authorizedData) => {
    if (!err) {
      authData = authorizedData;
    }
  });
  return authData;
};

module.exports = verifyToken;
