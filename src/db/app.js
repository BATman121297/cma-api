const dotenv = require("dotenv");
dotenv.config();

const identifyDb = require("./identify-db");

// ##################

const services = Object.freeze({
  identifyDb
});

module.exports = services;
module.exports = {
  identifyDb
};
