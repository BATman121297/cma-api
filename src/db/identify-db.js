const identifyDb = ({ env }) => {
  return async function selects() {
    try {
      if (env === "dev") {
        const res = {
          DB: "truck_scale_dev",
          SL: "DEVBFI_TRUCKSCALE"
        };
        return res;
      }

      if (env === "test") {
        return "truck_scale_test";
      }

      if (env === "uat") {
        return "truck_scale_uat";
      }

      if (env === "prod") {
        return "truck_scale_prod";
      }

      if (env === "sqa") {
        return "truck_scale_sqa";
      }
    } catch (e) {
      console.log(e);
    }
  };
};

module.exports = identifyDb;
