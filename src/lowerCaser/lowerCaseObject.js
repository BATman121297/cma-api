const lowerCaser = () => {
  return async function def(obj) {
    for (let i = 0; i < Object.entries(obj).length; i++) {
      if (typeof Object.entries(obj)[i][1] === "string") {
        if (Object.entries(obj)[i][0] !== "cookie" && Object.entries(obj)[i][0] !== "U_TS_EMAIL") {
          obj[Object.entries(obj)[i][0]] = Object.entries(obj)[
            i
          ][1].toLowerCase();
        }
      }
    }

    return obj;
  };
};

module.exports = lowerCaser;
