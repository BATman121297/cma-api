
const lowerCaser = require("./lowerCaseObject")

const objectLowerCaser = lowerCaser()

const services = Object.freeze({
    objectLowerCaser
});

module.exports = services;
module.exports = {
    objectLowerCaser
};