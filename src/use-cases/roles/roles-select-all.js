const rolesSelectAll = ({ rolesDb, decrypt, validateAccessRights, roles }) => {
  return async function selects(info) {

 
    const mode = info.mode;

    if(!info.modules){
      throw new Error (`Access denied`)
    }

    const allowed = await validateAccessRights(info.modules,'admin','view roles')


    if(!allowed){
      throw new Error (`Access denied`)
    }

    if(mode == 1){
      
      delete info.modules

      delete info.source;
      delete info.mode;


      const res = await roles.rolesSelectAll({info});

      return res 

    }else {


    const result = await rolesDb.selectAllRoles({});
    const roles = result.rows;

   
    let data = []; //declare empty array

    for await (let i of roles) {
      // created by
      const created_by = [
        {
          firstname: i.create_fn ? decrypt(i.create_fn) : i.create_fn,
          middlename: i.create_mn ? decrypt(i.create_mn) : i.create_mn,
          lastname: i.create_ln ? decrypt(i.create_ln): i.create_ln,
          created_at: i.created_at
        }
      ];

      // modified by
      const modified_by = [
        {
          firstname: i.modify_fn ? decrypt(i.modify_fn) : i.modify_fn,
          middlename: i.modify_mn ? decrypt(i.modify_mn) : i.modify_mn,
          lastname: i.modify_ln ? decrypt(i.modify_ln) : i.modify_ln,
          updated_at: i.updated_at
        }
      ];

      
      data.push({
        id: i.role_id,
        rolename: i.rolename,
        rolestatus: i.rolestatus,
        created_by,
        modified_by
      });
    }

    return data;
  }
  };
};

module.exports = rolesSelectAll;
