const updateRoles = ({
  rolesDb,
  makeRolesUpdate,
  validateAccessRights,
  addAccessRightss,
  insertActivityLogss,
  decrypt,
  roles,
  objectLowerCaser,
  accessRights,
  moment
}) => {
  return async function put({ id, ...info } = {}) {
    const { mode } = info;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "admin",
      "edit role"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;

      if (info.actions.length === 0) {
        throw new Error(`Please add actions`);
      }

      const actions = info.actions;
      const role_id = id;
      const cookie = info.cookie;
      // let array = []

      delete info.mode;
      delete info.source;
      delete info.actions;

      info.id = id;

      info = await objectLowerCaser(info);

      val({ info });

      const check = await roles.rolesAddSelectByName({ info });

      const length = check.length;

      if (length > 0) {
        throw new Error(`Role name already exists.`);
      }

      // update the role itself;
      const res = await roles.rolesUpdate({ info });
      if (res.status === 204) {
        //teardown
        const codes = await accessRights.arSelectAllCode({ info });

        // append all batch string; no tab
        let batchString = `
--a
Content-Type:multipart/mixed;boundary=b
        `;

        for (let i = 0; i < codes.length; i++) {
          const id = codes[i].Code;
          // append delete batch; no tab
          const string = `
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
DELETE /b1s/v1/U_BFI_TS_ACCRGHT('${id}')
          `;

          batchString += string;
        }

        // get max code
        // get max code; for auto increment
        const max = await accessRights.arGetMaxCode({});
        let maxCode = 0;
        if (max[0].castmax == null) {
          maxCode++;
        } else {
          // increment by 1
          maxCode = max[0].castmax + 1;
        }

        // get new actions
        for (let i = 0; i < actions.length; i++) {
          const d = new Date().toDateString();
          const date = moment(d).format("YYYY-MM-DD");
          const t = new Date().toTimeString();
          const time = await fixTime(t);

          const e = actions[i];

          const string = `\n
--b
Content-Type:application/http
Content-Transfer-Encoding:binary
POST /b1s/v1/U_BFI_TS_ACCRGHT

{
"Code": "${maxCode}",
"Name": "${maxCode}",
"U_TS_ACTION_ID": "${e}",
"U_TS_ROLE_ID": "${role_id}",
"U_TS_CREATEDATE": "${date}",
"U_TS_CREATETIME": ${time}
}
          `;

          maxCode++;
          batchString += string;
        }

        batchString += `
--b--
--a--
        `;

        const batchData = {
          batchString,
          cookie
        };
        // batch request; update access rights
        const res = await accessRights.batchRequest(batchData);
        const status = res.status;
        if (status == 202) {
          // success batch
          const d = {
            status,
            msg: "Updated successfully."
          };
          return JSON.stringify(d);
        } else {
          // error batch
          const d = {
            status,
            msg: "Something went wrong, please try again."
          };
          throw new Error(JSON.stringify(d));
        }

        // for (let i = 0; i < codes.length; i++) {
        //   await accessRights.arDelete({
        //     id: codes[i].Code,
        //     cookie: cookie
        //   });
        // }

        // // setup
        // for (let i = 0; i < actions.length; i++) {
        //   // get max code; for auto increment
        //   const max = await accessRights.arGetMaxCode({});
        //   let maxCode = 0;
        //   if (max[0].castmax == null) {
        //     maxCode++;
        //   } else {
        //     // increment by 1
        //     maxCode = max[0].castmax + 1;
        //   }

        //   await accessRights.arAdd({
        //     Code: maxCode,
        //     Name: maxCode,
        //     U_TS_ACTION_ID: actions[i],
        //     U_TS_ROLE_ID: role_id,
        //     cookie: cookie
        //   });
        // }
      }
    } else {
      // offline mode
      const result = makeRolesUpdate(info);

      const roleExist = await rolesDb.selectByRoleNameUpdate({
        name: result.getRoleName(),
        id
      });

      if (roleExist.rowCount !== 0) {
        if (result.getRoleName !== roleExist.name) {
          throw new Error("The role name already exist.");
        }
      }

      // query previous values
      const previous = await rolesDb.selectOneRole({ id });
      const prev_data = previous.rows[0];
      const prev_values = {
        role_id: prev_data.role_id,
        rolename: prev_data.rolename,
        rolestatus: prev_data.rolestatus,
        created_by: {
          create_fn: prev_data.create_fn ? decrypt(prev_data.create_fn) : "",
          create_mn: prev_data.create_mn ? decrypt(prev_data.create_mn) : "",
          create_ln: prev_data.create_ln ? decrypt(prev_data.create_ln) : "",
          created_at: prev_data.created_at
        },
        modified_by: {
          modify_fn: prev_data.modify_fn ? decrypt(prev_data.modify_fn) : "",
          modify_mn: prev_data.modify_mn ? decrypt(prev_data.modify_mn) : "",
          modify_ln: prev_data.modify_ln ? decrypt(prev_data.modify_ln) : "",
          updated_at: prev_data.updated_at
        }
      };

      // update to db
      const update = await rolesDb.updateRoles({
        name: result.getRoleName(),
        status: result.getRoleStatus(),
        modified_by: result.getModifiedBy(),
        id,
        updated_at: result.getUpdatedAt()
      });

      const count = update.rowCount; // get the number of inserted data

      const data = {
        msg: `Updated successfully ${count} role.`
      };

      // add access rights
      await addAccessRightss({
        id: id,
        access_rights: info.access_rights
      });

      const user = await rolesDb.returnCreatedBy({
        id: result.getModifiedBy()
      });
      const create = user.rows[0];
      const modified_by = {
        id: create.id,
        employee_id: create.employee_id ? decrypt(create.employee_id) : "",
        firstname: create.firstname ? decrypt(create.firstname) : "",
        lastname: create.lastname ? decrypt(create.lastname) : ""
      };

      // logs
      // new values
      const new_values = {
        name: result.getRoleName(),
        status: result.getRoleStatus(),
        modified_by,
        updated_at: result.getUpdatedAt()
      };

      const logs = {
        action_type: "UPDATE ROLE",
        table_affected: "ts_roles",
        new_values,
        prev_values,
        created_at: null,
        updated_at: new Date().toISOString(),
        users_id: result.getModifiedBy()
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};

const val = ({ info }) => {
  const { U_TS_NAME, U_TS_STATUS, U_TS_MODIFYBY } = info;

  if (!U_TS_NAME) {
    const d = {
      msg: "Please enter role name."
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_MODIFYBY) {
    const d = {
      msg: "Please enter who modified the role."
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_STATUS) {
    const d = {
      msg: "Please enter status."
    };
    throw new Error(JSON.stringify(d));
  }
};

const fixTime = t => {
  try {
    const arr = t.split(":", 2);
    const raw = `${arr[0]}${arr[1]}`;
    const time = raw;
    return time;
  } catch (e) {
    console.log("Error: ", e);
  }
};

module.exports = updateRoles;
