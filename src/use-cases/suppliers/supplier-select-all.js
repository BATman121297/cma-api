const suppliersSelectAll = ({ suppliersDb }) => {
  return async function selects() {
    const result = await suppliersDb.selectAllSupplier();
    const suppliers = result.rows;

    return suppliers;
  };
};

module.exports = suppliersSelectAll;
