const addNewSupplier = ({ suppliersDb, makeSupplier, insertActivityLogss }) => {
  return async function posts(info) {
    const result = makeSupplier(info);

    const supplierExist = await suppliersDb.selectByName({
      name: result.getSupplierName()
    });

    if (supplierExist.rowCount !== 0) {
      throw new Error("The supplier's name already exist.");
    }

    // insert to db
    const insert = await suppliersDb.insertNewSupplier({
      name: result.getSupplierName(),
      address: result.getAddress(),
      contact_number: result.getContact()
    });

    const count = insert.rowCount; // get the number of inserted data

    const data = {
      msg: `Inserted successfully ${count} supplier.`
    };

    // logs
    // new values
    const new_values = {
      name: result.getSupplierName(),
      address: result.getAddress(),
      contact_number: result.getContact()
    };

    const logs = {
      action_type: "CREATE SUPPLIER",
      table_affected: "ts_suppliers",
      new_values,
      prev_values: null,
      created_at: new Date().toISOString(),
      updated_at: null,
      users_id: info.users_id
    };

    await insertActivityLogss({ logs });

    return data;
  };
};

module.exports = addNewSupplier;
