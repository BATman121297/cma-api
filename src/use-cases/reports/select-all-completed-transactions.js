const completedTransactionSelectAll = ({
  reportsDb,
  decrypt,
  transactions,
  transactionTypes,
  weightTypes,
  drivers,
  truckInfos,
  validateAccessRights,
  users
}) => {
  return async function selects(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "report",
      "view reports"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;
      delete info.source;
      delete info.mode;

      let finalView = [];

      if (!info.from || !info.to) {
        throw new Error(`Please input date from and to`);
      }

      let processedCode = [];

      const completedTransactions = await transactions.transactionsSelectAllCompletedTransactions(
        { info }
      );

      if (completedTransactions.length > 0) {
        for (let i = 0; i < completedTransactions.length; i++) {
          let data = completedTransactions[i];
          //special case
          if (processedCode.includes(data.DocEntry)) {
            const index = finalView.findIndex(x => x.id === data.DocEntry);

            finalView[index].weightDetails.push({
              ibWeight: data.IB_WEIGHT,
              ibUpdateDate: data.IB_UPDATE_DATE,
              ibUpdateTime: data.IB_UPDATE_TIME,
              ibWeigher: data.IB_WEIGHER,
              obWeight: data.OB_WEIGHT,
              obRemarks: data.OB_REMARKS,
              obUpdateDate: data.OB_UPDATE_DATE,
              obUpdateTime: data.OB_UPDATE_TIME,
              obSign: data.OB_SIGN,
              obWeigher: data.OB_WEIGHER
            });
          } else {
            let weights = [];

            weights.push({
              ibWeight: data.IB_WEIGHT,
              ibUpdateDate: data.IB_UPDATE_DATE,
              ibUpdateTime: data.IB_UPDATE_TIME,
              ibWeigher: data.IB_WEIGHER,
              obWeight: data.OB_WEIGHT,
              obRemarks: data.OB_REMARKS,
              obUpdateDate: data.OB_UPDATE_DATE,
              obUpdateTime: data.OB_UPDATE_TIME,
              obSign: data.OB_SIGN,
              obWeigher: data.OB_WEIGHER
            });

            finalView.push({
              id: data.DocEntry,
              createDate: data.CREATEDATE,
              createTime: data.CreateTime,
              status: data.U_TS_STATUS,
              transactionType: data.U_TS_TRNSTYPE,
              driverName: data.DRIVER_NAME,
              plateNumber: data.U_TS_PLATE_NUM,
              truckType: data.U_TS_TRTYPE,
              truckModel: data.U_TS_TRMODEL,
              supplier: data.CardName,
              supplierAddress: data.Address,
              items: data.ITEM_LIST,
              trackingCode: data.U_TS_TRCK_CODE,
              numberOfBags: data.U_TS_NUM_BAGS,
              isNapier: data.U_TS_IS_NAPIER,
              weightDetails: weights
            });
          }

          processedCode.push(data.DocEntry);
        }
      } else {
        throw new Error("No data to display.");
      }

      return finalView;
    } else {
      // offline mode
      const result = await reportsDb.selectAllCompletedTransaction({
        from: info.from,
        to: info.to
      });
      const report = result.rows;

      let data = [];
      let res = [];
      // populate all data
      for await (let i of report) {
        const transactionId = i.id; // transaction id

        const weigher = {
          firstname: decrypt(i.wfirstname),
          lastname: decrypt(i.wlastname)
        };

        let inbound = [];
        let outbound = [];
        const driver = {
          firstname: decrypt(i.firstname),
          lastname: decrypt(i.lastname)
        };

        inbound.push({
          id: i.inbound_id ? i.inbound_id : "N/A",
          transactionId: i.transaction_id_ib ? i.transaction_id_ib : "N/A",
          weight: i.ib_weight ? i.ib_weight : 0,
          timestamp: i.ib_timestamp ? i.ib_timestamp : "N/A"
        });
        outbound.push({
          id: i.outbound_id ? i.outbound_id : "N/A",
          transactionId: i.transaction_id_ob ? i.transaction_id_ob : "N/A",
          weight: i.ob_weight ? i.ob_weight : 0,
          timestamp: i.ob_timestamp ? i.ob_timestamp : "N/A",
          inbound_id: i.ob_inbound_id ? i.ob_inbound_id : "N/A"
        });

        if (data.length > 0) {
          const previousTransactionId = data[data.length - 1].id; // previous transaction
          // if the same transaction id; for delivery
          if (transactionId == previousTransactionId) {
            // append inbound weight
            data[data.length - 1].inbound.push({
              id: i.inbound_id ? i.inbound_id : "N/A",
              transactionId: i.transaction_id_ib ? i.transaction_id_ib : "N/A",
              weight: i.ib_weight ? i.ib_weight : 0,
              timestamp: i.ib_timestamp ? i.ib_timestamp : "N/A"
            });

            // append outbound weight
            data[data.length - 1].outbound.push({
              id: i.outbound_id ? i.outbound_id : "N/A",
              transactionId: i.transaction_id_ob ? i.transaction_id_ob : "N/A",
              weight: i.ob_weight ? i.ob_weight : 0,
              timestamp: i.ob_timestamp ? i.ob_timestamp : "N/A"
            });
          } else {
            await data.push({
              id: i.id,
              transaction_type: i.transaction_type_name,
              created_at: i.created_at,
              purchase_order_id: i.purchase_order_id,
              tracking_code: i.tracking_code,
              transmittal_number: i.transmittal_number,
              no_of_bags: i.no_of_bags,
              raw_material: i.description,
              supplier: i.supplier_name,
              weigher,
              driver,
              plate_number: i.plate_number,
              status: i.status,
              inbound,
              outbound
            });
          }
        } else {
          await data.push({
            id: i.id,
            transaction_type: i.transaction_type_name,
            created_at: i.created_at,
            purchase_order_id: i.purchase_order_id,
            tracking_code: i.tracking_code,
            transmittal_number: i.transmittal_number,
            no_of_bags: i.no_of_bags,
            raw_material: i.description,
            supplier: i.supplier_name,
            weigher,
            driver,
            plate_number: i.plate_number,
            status: i.status,
            inbound,
            outbound
          });
        }
      }

      // filter data
      for await (let i of data) {
        await res.push({
          id: i.id,
          transaction_type: i.transaction_type,
          created_at: i.created_at,
          purchase_order_id: i.purchase_order_id,
          tracking_code: i.tracking_code,
          transmittal_number: i.transmittal_number,
          no_of_bags: i.no_of_bags,
          raw_material: i.raw_material,
          supplier: i.supplier,
          weigher: i.weigher,
          driver: i.driver,
          plate_number: i.plate_number,
          status: i.status,
          inbound: i.inbound ? filterArrayNoDuplicates(i.inbound, "id") : null,
          outbound: i.outbound
            ? filterArrayNoDuplicates(i.outbound, "id")
            : null
        });
      }

      return res;
    }
  };
};

// function to remove duplicate elements in array
// arr = is the array
// comp = is the component you want it to filter with
const filterArrayNoDuplicates = (arr, comp) => {
  const unique = arr
    .map(e => e[comp])

    // store the keys of the unique objects
    .map((e, i, final) => final.indexOf(e) === i && i)

    // eliminate the dead keys & store unique objects
    .filter(e => arr[e])
    .map(e => arr[e]);

  return unique;
};

module.exports = completedTransactionSelectAll;
