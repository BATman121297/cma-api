const reportsDb = require("../../data-access/db-layer/reports/app"); //db
const { decrypt } = require("../../../crypting/app"); //decrypt

const { transactions } = require("../../data-access/sl-layer/transactions/app");
const { transactionTypes } = require("../../data-access/sl-layer/transaction-type/app")
const { weightTypes } = require("../../data-access/sl-layer/weight-types/app")
const { drivers } = require("../../data-access/sl-layer/drivers/app");
const { truckInfos } = require("../../data-access/sl-layer/truck-infos/app");
const { users } = require("../../data-access/sl-layer/users/app");

const { validateAccessRights } = require("../../validator/app"); //validator

//####################
const completedTransactionSelectAll = require("./select-all-completed-transactions");
//####################
const completedTransactionSelectAlls = completedTransactionSelectAll({
  reportsDb,
  decrypt,
  transactions,
  transactionTypes,
  weightTypes,
  drivers,
  truckInfos,
  users,
  validateAccessRights
});

const services = Object.freeze({ completedTransactionSelectAlls });

module.exports = services;
module.exports = { completedTransactionSelectAlls };
