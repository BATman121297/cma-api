const addNewTruckScale = ({
  truckScaleDb,
  makeTruckScale,
  insertActivityLogss,
  truckscale,
  objectLowerCaser,
  returnIpAddress,
  hostName
}) => {
  return async function posts(info) {
    const mode = info.mode;

    if (mode == 1) {
      delete info.source;
      delete info.mode;

      info.U_TS_TR_IP = returnIpAddress();
      info.U_TS_DEVICE = hostName();

      info = await objectLowerCaser(info);

      val({ info });

      const max = await truckscale.truckscaleGetMaxCode({});
      const maxCode = max[0].maxCode;

      info.Code = maxCode;
      info.Name = maxCode;

      const check = await truckscale.truckscaleAddSelectByIPAndDeviceName({
        info
      });

      const length = check.data.length;

      if (length > 0) {
        throw new Error(`Device already exists.`);
      }

      const res = await truckscale.truckscaleAdd({ info });

      const reqStatus = res.status;
      if (reqStatus == 201) {
        return res;
      } else {
        // throw error from SL
        throw new Error(res.data.error);
      }
    } else {
      const result = makeTruckScale(info);

      const tsExist = await truckScaleDb.selectTruckScaleName({
        truckscale_name: result.getTSName()
      });

      if (tsExist.rowCount !== 0) {
        throw new Error("The truck scale name already exist.");
      }

      // instead of checking just delete the existing data then insert
      // delete existing data then insert
      // ==== reconsider this; if delete or not
      // await truckScaleDb.deleteExistingData({
      //   device_name: result.getHostName(),
      //   ip_address: result.getIp()
      // });

      // insert to db
      const insert = await truckScaleDb.insertNewTruckScale({
        truckscale_name: result.getTSName(),
        truckscale_location: result.getTSLocation(),
        truckscale_length: result.getTSLength(),
        ip_address: result.getIp(),
        device_name: result.getHostName(),
        baudrate: result.getBaudRate(),
        parity: result.getParity(),
        databits: result.getDataBits(),
        stopbits: result.getStopBits(),
        created_at: result.getCreatedAt()
      });

      const count = insert.rowCount; // get the number of inserted data

      const data = {
        msg: `Inserted successfully ${count} truck scale.`
      };

      // logs
      // new values
      const new_values = {
        truckscale_name: result.getTSName(),
        truckscale_location: result.getTSLocation(),
        truckscale_length: result.getTSLength(),
        ip_address: result.getIp(),
        device_name: result.getHostName(),
        baudrate: result.getBaudRate(),
        parity: result.getParity(),
        databits: result.getDataBits(),
        stopbits: result.getStopBits(),
        created_at: result.getCreatedAt()
      };

      const logs = {
        action_type: "CREATE TRUCKSCALE",
        table_affected: "ts_truckscales",
        new_values,
        prev_values: null,
        created_at: new Date().toISOString(),
        updated_at: null,
        users_id: info.users_id
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};

const val = ({ info }) => {
  const {
    U_TS_TRSCALE,
    U_TS_TR_LOCATION,
    U_TS_TR_LENGTH,
    U_TS_BAUDRATE
  } = info;

  if (!U_TS_TRSCALE) {
    const d = {
      msg: "Please enter truckscale name."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_TR_LOCATION) {
    const d = {
      msg: "Please enter truckscale location."
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_TR_LENGTH) {
    const d = {
      msg: "Please enter truckscale length."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_BAUDRATE) {
    const d = {
      msg: "Please enter baudrate."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = addNewTruckScale;
