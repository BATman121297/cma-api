const tsSelectAll = ({ truckScaleDb, truckscale }) => {
  return async function selects(info) {
    const mode = info.mode;

    if (mode == 1) {
      delete info.source;
      delete info.mode;

      const res = await truckscale.truckscaleSelectAll({ info });
      if (res.status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }
      return res;
    } else {
      const result = await truckScaleDb.selectAllTruckScale({});
      const ts = result.rows;

      return ts;
    }
  };
};

module.exports = tsSelectAll;
