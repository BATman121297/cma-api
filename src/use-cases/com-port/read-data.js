const readSerialData = ({
  serialComPorts,
  Readline,
  io,
  port,
  closeSerialPort,
  count
}) => {
  return async function get(info) {
    try {
      // first time connecting so
      // just connect to serial port
      if (count === 0) {
        port = await serialComPorts({ info });
        const parser = port.pipe(new Readline({ delimiter: "\r" })); //Read the line only when new line comes.
        parser.on("data", num => {
          //Read data
          io.sockets.emit("num", {
            num
          }); // emit data to all connected clients
        });
        count++;

        const msg = { msg: `Connected successfully to ${port.path}.` };
        return msg;
      } else {
        // already connected; disconnect then connect
        const closeSerial = await closeSerialPort({ port });
        const close = await closeSerial({});
        if (close) {
          setTimeout(async () => {
            // connect again
            port = await serialComPorts({ info });
            const parser = port.pipe(new Readline({ delimiter: "\r" })); //Read the line only when new line comes.
            parser.on("data", num => {
              //Read data
              io.sockets.emit("num", {
                num
              }); // emit data to all connected clients
            });
          }, 100);
        } else {
          setTimeout(async () => {
            // connect again
            port = await serialComPorts({ info });
            const parser = port.pipe(new Readline({ delimiter: "\r" })); //Read the line only when new line comes.
            parser.on("data", num => {
              //Read data
              io.sockets.emit("num", {
                num
              }); // emit data to all connected clients
            });
          }, 100);
        }

        const msg = {
          close: close ? `${close.msg}` : "",
          msg: `Connected successfully to ${info.comPort}.`
        };

        return msg;
      }
    } catch (e) {
      const err = {
        msg: "No device found."
      };
      throw new Error(JSON.stringify(err));
    }
  };
};

module.exports = readSerialData;
