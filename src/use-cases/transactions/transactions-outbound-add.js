const addOutbound = ({
  transactionDb,
  weightDb,
  e_addOutbound,
  insertActivityLogss,
  transactions,
  transactionTypes,
  weightTypes,
  validateAccessRights
}) => {
  return async function post(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "transaction",
      "edit transaction"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;
      delete info.source;
      delete info.mode;

      //get transactions
      const transactionDetails = await transactions.transactionsSelectOne({
        info
      });

      //throw error if transaction does not exist
      if (transactionDetails.status === 404) {
        throw new Error(`Transaction does not exist.`);
      }

      const transactionId = transactionDetails.data.DocEntry;

      //get transaction type id
      const transactionTypeId = transactionDetails.data.U_TS_TRNS_TYPE;

      //get transaction type details
      const transactionTypeDetails = await transactionTypes.transactionTypesSelectById(
        {
          Code: transactionTypeId,
          cookie: info.cookie
        }
      );

      //get transaction type description
      transactionTypeDescription = transactionTypeDetails.data[0].U_TS_TRNSTYPE;

      //if transaction is others
      if (transactionTypeDescription === "others") {
        //to do validation
        if (!info.ob_weight) {
          throw new Error(`Please input outbound weight`);
        }
        if (!info.updated_date) {
          throw new Error(`Please input date updated`);
        }
        if (!info.updated_date) {
          throw new Error(`Please input time updated`);
        }
        if (!info.sign) {
          throw new Error(`Please input signature`);
        }

        //get line id
        const outboundId =
          transactionDetails.data.BFI_TS_OBTRNCollection[
            transactionDetails.data.BFI_TS_OBTRNCollection.length - 1
          ].LineId;

        //build object for sap
        const outboundDetails = {
          id: transactionId,
          U_TS_STATUS: "completed",
          BFI_TS_OBTRNCollection: [
            {
              LineId: outboundId,
              U_TS_UPDATEDATE: info.updated_date,
              U_TS_UPDATETIME: info.updated_time,
              U_TS_OB_WEIGHT: info.ob_weight,
              U_TS_REMARKS: info.remarks,
              U_TS_OB_SIGN: info.sign
            }
          ],
          cookie: info.cookie
        };

        const res = await transactions.transactionsUpdate(outboundDetails);
        return res;
      } else if (
        transactionTypeDescription === "withdrawal" ||
        transactionTypeDescription === "delivery"
      ) {
        //to do validation
        if (!info.ob_weight) {
          throw new Error(`Please input outbound weight`);
        }
        if (!info.updated_date) {
          throw new Error(`Please input date updated`);
        }
        if (!info.updated_date) {
          throw new Error(`Please input time updated`);
        }
        if (!info.truckscale_id) {
          throw new Error(`Please input truckscale ID`);
        }
        if (!info.sign) {
          throw new Error(`Please input signature`);
        }

        // check if there is PO attached or not;
        // dont allow outbound if there is no PO
        // remove this
        // const po = transactionDetails.data.U_APP_PO_ID;
        // if (!po) {
        //   const d = {
        //     code: 001,
        //     msg: "There must be a PO attached, try again next time."
        //   };
        //   throw new Error(JSON.stringify(d));
        // }

        //get line id
        const outboundId =
          transactionDetails.data.BFI_TS_OBTRNCollection[
            transactionDetails.data.BFI_TS_OBTRNCollection.length - 1
          ].LineId;

        //build object for sap
        const outboundDetails = {
          id: transactionId,
          U_TS_STATUS: "completed",
          BFI_TS_OBTRNCollection: [
            {
              LineId: outboundId,
              U_TS_UPDATEDATE: info.updated_date,
              U_TS_UPDATETIME: info.updated_time,
              U_TS_OB_WEIGHT: info.ob_weight,
              U_TS_TRKSCL_ID: info.truckscale_id,
              U_TS_OB_SIGN: info.sign,
              U_TS_REMARKS: info.remarks
            }
          ],
          cookie: info.cookie
        };

        const res = await transactions.transactionsUpdate(outboundDetails);
        return res;
      }
    } else {
      // check if there is transaction id; doesn't pass thru entity
      if (!info.transaction_id) {
        throw new Error(`Please enter transaction ID.`);
      }

      const transactionId = info.transaction_id;

      //get transaction details
      const transactionInfo = await transactionDb.selectOneTransactionById({
        id: transactionId
      });

      // check if transaction exist
      if (transactionInfo.rowCount === 0) {
        throw new Error(`Transaction does not exist.`);
      }

      // status of the transaction
      const status = transactionInfo.rows[0].status.toLowerCase();

      // transaction type
      const transactionType = transactionInfo.rows[0].transaction_type_name.toLowerCase();
      // if delivery set to tare
      if (transactionType === "delivery") {
        if (status === "completed") {
          // when completed; insert new outbound record together with the inbound id
          // because there is an inbound record already
          const entity = await e_addOutbound(info);

          // get the lates inbound id
          const inbound = await transactionDb.returnLatestInboundFromTransactionId(
            {
              id: transactionId
            }
          );

          if (inbound.rowCount > 0) {
            const inbound_id = inbound.rows[0].id; // update this to outbound record

            // weight type
            const weightType = await weightDb.selectByName({
              weight_name: "Tare"
            });
            const weight_type_id = weightType.rows[0].id;

            const insert = await transactionDb.addOutboundTransaction({
              transaction_id: entity.getTransactionId(),
              weight_type_id,
              ob_weight: entity.getObWeight(),
              ob_timestamp: entity.getObTimestamp(),
              users_id: entity.getUsersId(),
              truckscale_id: entity.getTruckscaleId(),
              drivers_id: entity.getDriversId(),
              truck_info_id: entity.getTruckInfoId(),
              inbound_id,
              remarks: entity.getRemarks()
            });
            const count = insert.rowCount;

            // #################
            // logs
            // new values
            const new_values = {
              transaction_id: entity.getTransactionId(),
              weight_type_id,
              ob_weight: entity.getObWeight(),
              ob_timestamp: entity.getObTimestamp(),
              users_id: entity.getUsersId(),
              truckscale_id: entity.getTruckscaleId(),
              drivers_id: entity.getDriversId(),
              truck_info_id: entity.getTruckInfoId(),
              inbound_id,
              remarks: entity.getRemarks()
            };

            const logs = {
              action_type:
                "DELIVERY TRANSACTION SPECIAL CASE - INSERT OUTBOUND",
              table_affected: "ts_transactions",
              new_values,
              prev_values: null,
              created_at: new Date().toISOString(),
              updated_at: null,
              users_id: entity.getUsersId()
            };

            await insertActivityLogss({ logs });
            // ################# logs

            const data = {
              msg: `Inserted successfully ${count} outbound transaction.`
            };

            return data;
          } else {
            throw new Error(`No inbound transaction found.`);
          }
        } else {
          if (!info.transaction_id) {
            throw new Error(`Please enter transaction ID.`);
          }
          if (!info.ob_weight) {
            throw new Error(`Please enter outbound weight.`);
          }
          // check if transaction id exist
          const transactionInfo = await transactionDb.selectOneTransactionById({
            id: info.transaction_id
          });

          if (transactionInfo.rowCount === 0) {
            throw new Error(`Transaction does not exist.`);
          }

          // get inbound id
          // get the lates inbound id
          const inbound = await transactionDb.returnLatestInboundFromTransactionId(
            {
              id: transactionId
            }
          );

          if (inbound.rowCount > 0) {
            const inbound_id = inbound.rows[0].id; // update this to outbound record

            const insert = await transactionDb.updateOutboundTransactionDelivery(
              {
                transaction_id: info.transaction_id,
                ob_weight: info.ob_weight,
                ob_timestamp: new Date().toISOString(),
                inbound_id
              }
            );

            const count = insert.rowCount;

            const transaction_id = info.transaction_id; // transaction id
            // update status
            const status = `completed`;
            await transactionDb.updateTransactionStatus({
              id: transaction_id,
              status
            });

            const data = {
              msg: `Inserted successfully ${count} outbound transaction.`
            };

            // #################
            // logs
            // new values
            const new_values = {
              transaction_id: info.transaction_id,
              ob_weight: info.ob_weight,
              ob_timestamp: new Date().toISOString(),
              inbound_id
            };

            const prev_values = {
              transaction_id: info.transaction_id,
              ob_weight: null,
              ob_timestamp: null,
              inbound_id: null
            };

            const logs = {
              action_type: "DELIVERY TRANSACTION - UPDATE OUTBOUND",
              table_affected: "ts_transactions",
              new_values,
              prev_values,
              created_at: null,
              updated_at: new Date().toISOString(),
              users_id: info.users_id
            };

            await insertActivityLogss({ logs });
            // ################# logs

            return data;
          } else {
            throw new Error(`No inbound transaction found.`);
          }
        }
      }

      // if transaction type is withdrawal set to gross
      if (transactionType === "withdrawal") {
        if (status === "completed") {
          throw new Error(`Withdrawal transaction is completed.`);
        } else {
          if (!info.transaction_id) {
            throw new Error(`Please enter transaction ID.`);
          }
          if (!info.ob_weight) {
            throw new Error(`Please enter outbound weight.`);
          }
          // check if transaction id exist
          const transactionInfo = await transactionDb.selectOneTransactionById({
            id: info.transaction_id
          });

          if (transactionInfo.rowCount === 0) {
            throw new Error(`Transaction does not exist.`);
          }

          // update
          const inbound = await transactionDb.returnLatestInboundFromTransactionId(
            {
              id: transactionId
            }
          );

          if (inbound.rowCount > 0) {
            const inbound_id = inbound.rows[0].id; // update this to outbound record

            const insert = await transactionDb.updateOutboundTransactionDelivery(
              {
                transaction_id: info.transaction_id,
                ob_weight: info.ob_weight,
                ob_timestamp: new Date().toISOString(),
                inbound_id
              }
            );

            const count = insert.rowCount;

            const transaction_id = info.transaction_id; // transaction id
            // update status
            const status = `completed`;
            await transactionDb.updateTransactionStatus({
              id: transaction_id,
              status
            });

            const data = {
              msg: `Inserted successfully ${count} outbound transaction.`
            };

            // #################
            // logs
            // new values
            const new_values = {
              transaction_id: info.transaction_id,
              ob_weight: info.ob_weight,
              ob_timestamp: new Date().toISOString(),
              inbound_id
            };

            const prev_values = {
              transaction_id: info.transaction_id,
              ob_weight: null,
              ob_timestamp: null,
              inbound_id: null
            };

            const logs = {
              action_type: "WITHDRAWAL TRANSACTION - UPDATE OUTBOUND",
              table_affected: "ts_transactions",
              new_values,
              prev_values,
              created_at: null,
              updated_at: new Date().toISOString(),
              users_id: info.users_id
            };

            await insertActivityLogss({ logs });
            // ################# logs

            return data;
          }
        }
      }

      // if transaction type is others
      if (transactionType === "others") {
        const weightType = await weightDb.selectByName({ weight_name: "Tare" });

        // check if there is ib weight
        if (!info.ob_weight) {
          throw new Error(`Please enter inbound weight.`);
        }
        // check if  there is truckscale id
        if (!info.truckscale_id) {
          throw new Error(`Please enter truckscale.`);
        }
        const ob_weight = info.ob_weight;
        const truckscale_id = info.truckscale_id;
        const transaction_id = info.transaction_id;
        const ob_timestamp = new Date().toISOString();
        const weight_type_id = weightType.rows[0].id;

        const inbound = await transactionDb.returnLatestInboundFromTransactionId(
          {
            id: transaction_id
          }
        );
        const inbound_id = inbound.rows[0].id; // update this to outbound record

        // update to db
        const insert = await transactionDb.updateOutboundTransaction({
          transaction_id,
          weight_type_id,
          ob_weight,
          ob_timestamp,
          inbound_id,
          truckscale_id
        });

        const count = insert.rowCount;

        // update status
        const status = `completed`;
        await transactionDb.updateTransactionStatus({
          id: transaction_id,
          status
        });

        const data = {
          msg: `Inserted successfully ${count} outbound transaction.`
        };

        // #################
        // logs
        // new values
        const new_values = {
          transaction_id,
          weight_type_id,
          ob_weight,
          ob_timestamp,
          inbound_id,
          truckscale_id
        };

        const prev_values = {
          transaction_id,
          weight_type_id: null,
          ob_weight: null,
          ob_timestamp: null,
          inbound_id: null,
          truckscale_id: null
        };

        const logs = {
          action_type: "OTHERS TRANSACTION - UPDATE OUTBOUND",
          table_affected: "ts_transactions",
          new_values,
          prev_values,
          created_at: null,
          updated_at: new Date().toISOString(),
          users_id: info.users_id
        };

        await insertActivityLogss({ logs });
        // ################# logs

        return data;
      }
    }
  };
};

module.exports = addOutbound;
