const addInbound = ({
  transactionDb,
  weightDb,
  e_addInbound,
  e_addOutbound,
  insertActivityLogss,
  transactions,
  transactionTypes,
  weightTypes,
  validateAccessRights
}) => {
  return async function post(info) {
    // check if there is transaction id; doesn't pass thru entity

    const mode = info.mode

    if(!info.modules){
      throw new Error (`Access denied`)
    }

    const allowed = await validateAccessRights(info.modules,'transaction','edit transaction')


    if(!allowed){
      throw new Error (`Access denied`)
    }

    if(mode == 1){

      const unacho = info.unacho
      delete info.unacho
      delete info.modules
      delete info.source;
      delete info.mode;

      //get transactions
      const transactionDetails = await transactions.transactionsSelectOne({info})

      //throw error if transaction does not exist
      if(transactionDetails.status === 404){
        throw new Error(`Transaction does not exist.`)
      }

      const transactionId = transactionDetails.data.DocEntry

      //get transaction type id
      const transactionTypeId = transactionDetails.data.U_TS_TRNS_TYPE

      

      //get transaction type details
      const transactionTypeDetails = await transactionTypes.transactionTypesSelectById({
        Code: transactionTypeId,
        cookie: info.cookie
      })

      //get transaction type description
      transactionTypeDescription = transactionTypeDetails.data[0].U_TS_TRNSTYPE

      //if transaction is others
      if(transactionTypeDescription === 'others'){
        //to do validation
        if(!info.ib_weight){
          throw new Error(`Please input inbound weight`)
        }
        if(!info.updated_date){
          throw new Error(`Please input date updated`)
        }
        if(!info.updated_date){
          throw new Error(`Please input time updated`)
        }
        // if(!info.sign){
        //   throw new Error(`Please input signature`)
        // }
        
        //get line id 
        const inboundId = transactionDetails.data.BFI_TS_IBTRNCollection[(transactionDetails.data.BFI_TS_IBTRNCollection).length-1].LineId

        let inboundDetails
        if(unacho){

          inboundDetails = {
            id: transactionId,
            U_TS_STATUS: "for outbound",
            BFI_TS_IBTRNCollection: [
                {
                    LineId: inboundId,
                    U_TS_UPDATEDATE: info.updated_date,
                    U_TS_UPDATETIME: info.updated_time,
                    U_TS_IB_WEIGHT: info.ib_weight,
                    // U_TS_IB_SIGN: info.sign
                }
            ],
            cookie: info.cookie
        }


        }else {

        //build object for sap
         inboundDetails = {
            id: transactionId,
            BFI_TS_IBTRNCollection: [
                {
                    LineId: inboundId,
                    U_TS_UPDATEDATE: info.updated_date,
                    U_TS_UPDATETIME: info.updated_time,
                    U_TS_IB_WEIGHT: info.ib_weight,
                    // U_TS_IB_SIGN: info.sign
                }
            ],
            cookie: info.cookie
        }
      }

        const res = await transactions.transactionsUpdate( inboundDetails );
        return res;
        
      }else if (transactionTypeDescription === "withdrawal" || transactionTypeDescription === "delivery"){
         //to do validation
         if(!info.ib_weight){
          throw new Error(`Please input inbound weight`)
        }
        if(!info.updated_date){
          throw new Error(`Please input date updated`)
        }
        if(!info.updated_date){
          throw new Error(`Please input time updated`)
        }
        if(!info.truckscale_id){
          throw new Error(`Please input truckscale ID`)
        }
        // if(!info.sign){
        //   throw new Error(`Please input signature`)
        // }
        
        //get line id 
        const inboundId = transactionDetails.data.BFI_TS_IBTRNCollection[(transactionDetails.data.BFI_TS_IBTRNCollection).length-1].LineId

        //build object for sap
        const inboundDetails = {
            id: transactionId,
            U_TS_STATUS: "for outbound",
            BFI_TS_IBTRNCollection: [
                {
                    LineId: inboundId,
                    U_TS_UPDATEDATE: info.updated_date,
                    U_TS_UPDATETIME: info.updated_time,
                    U_TS_IB_WEIGHT: info.ib_weight,
                    U_TS_TRKSCL_ID: info.truckscale_id,
                    // U_TS_IB_SIGN: info.sign
                }
            ],
            cookie: info.cookie
        }

        const res = await transactions.transactionsUpdate( inboundDetails );
        return res;

      }

    }else {

    if (!info.transaction_id) {
      throw new Error(`Please enter transaction ID.`);
    }

    const transactionId = info.transaction_id;
    //get transaction details
    const transactionInfo = await transactionDb.selectOneTransactionById({
      id: transactionId
    });

    //throw error if transaction does not exist
    if (transactionInfo.rowCount === 0) {
      throw new Error(`Transaction does not exist.`);
    }

    // status of the transaction
    const transactionStatus = transactionInfo.rows[0].status.toLowerCase();

    // check status
    if (
      transactionStatus === "others" ||
      transactionStatus === "for inbound" ||
      transactionStatus === "completed"
    ) {
      //  do nothing
    } else {
      throw new Error(`Invalid status.`);
    }

    // transaction type
    const transactionType = transactionInfo.rows[0].transaction_type_name.toLowerCase();

    // if delivery set to gross weight type
    if (transactionType === "delivery") {
      const weightType = await weightDb.selectByName({ weight_name: "Gross" }); //set weight type

      // when re-scan if status is completed; change to for outbound then record again
      // during outbound; ask the user if the same record to be store for the next inbound
      if (transactionStatus === "completed") {
        // special case; when completed and outbound again; update the inbound id to outbound record then
        // the outbound weight will be inserted to inbound;

        // during the outbound page; so body of req is outbound data..
        const entity = await e_addOutbound(info); // outbound entity

        // get the latest inbound id
        const inbound = await transactionDb.returnLatestInboundFromTransactionId(
          {
            id: transactionId
          }
        );
        const inbound_id = inbound.rows[0].id; // update this to outbound record

        // select last outbound transaction
        const outbound = await transactionDb.returnLatestOutboundFromTransactionId(
          {
            id: transactionId
          }
        );
        const outbound_id = outbound.rows[0].id; // use this id to update the inbound id

        // update inbound id
        await transactionDb.updateInboundIdToOutbound({
          id: outbound_id,
          inbound_id
        });

        const weight_type_id = weightType.rows[0].id;

        // record to inbound
        const insert = await transactionDb.addInboundTransaction({
          transaction_id: entity.getTransactionId(),
          weight_type_id,
          ib_weight: entity.getObWeight(),
          ib_timestamp: entity.getObTimestamp(),
          users_id: entity.getUsersId(),
          truckscale_id: entity.getTruckscaleId(),
          drivers_id: entity.getDriversId(),
          truck_info_id: entity.getTruckInfoId()
        });

        // #################
        // logs
        // new values
        const new_values = {
          transaction_id: entity.getTransactionId(),
          weight_type_id,
          ib_weight: entity.getObWeight(),
          ib_timestamp: entity.getObTimestamp(),
          users_id: entity.getUsersId(),
          truckscale_id: entity.getTruckscaleId(),
          drivers_id: entity.getDriversId(),
          truck_info_id: entity.getTruckInfoId()
        };

        const logs = {
          action_type: "DELIVERY TRANSACTION SPECIAL CASE - INSERT INBOUND",
          table_affected: "ts_transactions",
          new_values,
          prev_values: null,
          created_at: new Date().toISOString(),
          updated_at: null,
          users_id: entity.getUsersId()
        };

        await insertActivityLogss({ logs });
        // ################# logs

        const data = {
          msg: `Updated outbound transaction and inserted to inbound record.`
        };

        return data;
      } else {
        // status is for inbound
        // select inbound ID of latest transaction

        //get inbound transaction details
        const exist = await transactionDb.selectInboundToUpdate({
          transaction_id: info.transaction_id,
          transactionType
        });

        if (typeof exist.rows[0] !== "undefined") {
          // inbound id
          const inboundId = exist.rows[0].id;

          if (!info.ib_weight) {
            throw new Error(`Please enter inbound weight.`);
          }

          // update in db
          const ib_weight = info.ib_weight;
          const ib_timestamp = new Date().toISOString();

          const update = await transactionDb.updateInboundDeliveryTransaction({
            ib_weight,
            ib_timestamp,
            inboundId
          });

          // update status of transaction
          const status = `for outbound`; // status for outbound
          // update status
          await transactionDb.updateTransactionStatus({
            id: info.transaction_id,
            status
          });

          const count = update.rowCount;

          // #################
          // logs
          // new values
          const new_values = {
            ib_weight,
            ib_timestamp,
            inboundId
          };

          const prev_values = {
            ib_weight: null,
            ib_timestamp: null,
            inboundId
          };
          const logs = {
            action_type: "UPDATE DELIVERY TRANSACTION - INBOUND",
            table_affected: "ts_transactions",
            new_values,
            prev_values,
            created_at: null,
            updated_at: new Date().toISOString(),
            users_id: info.users_id
          };

          await insertActivityLogss({ logs });
          // ################# logs

          const data = {
            msg: `Inserted successfully ${count} inbound transaction.`
          };

          return data;
        } else {
          throw new Error(`Inbound transaction is already done.`);
        }
      }
    }

    // if withdrawal set to tare weight type
    if (transactionType === "withdrawal") {
      // status is for inbound
      // select inbound ID of latest transaction

      //get inbound transaction details
      const exist = await transactionDb.selectInboundToUpdate({
        transaction_id: info.transaction_id,
        transactionType
      });

      if (typeof exist.rows[0] !== "undefined") {
        // inbound id
        const inboundId = exist.rows[0].id;

        if (!info.ib_weight) {
          throw new Error(`Please enter inbound weight.`);
        }
        // update in db
        const ib_weight = info.ib_weight;
        const ib_timestamp = new Date().toISOString();

        const update = await transactionDb.updateInboundDeliveryTransaction({
          ib_weight,
          ib_timestamp,
          inboundId
        });

        // update status of transaction
        const status = `for outbound`; // status for outbound
        // update status
        await transactionDb.updateTransactionStatus({
          id: info.transaction_id,
          status
        });

        const count = update.rowCount;

        // #################
        // logs
        // new values
        const new_values = {
          ib_weight,
          ib_timestamp,
          inboundId
        };

        const prev_values = {
          ib_weight: null,
          ib_timestamp: null,
          inboundId
        };
        const logs = {
          action_type: "UPDATE WITHDRAWAL TRANSACTION - INBOUND",
          table_affected: "ts_transactions",
          new_values,
          prev_values,
          created_at: null,
          updated_at: new Date().toISOString(),
          users_id: info.users_id
        };

        await insertActivityLogss({ logs });
        // ################# logs

        const data = {
          msg: `Inserted successfully ${count} inbound transaction.`
        };

        return data;
      } else {
        throw new Error(`Inbound transaction is already done.`);
      }
    }

    // if others set to gross weight type
    if (transactionType === "others") {
      const weightType = await weightDb.selectByName({ weight_name: "Gross" });

      // check if there is ib weight
      if (!info.ib_weight) {
        throw new Error(`Please enter inbound weight.`);
      }
      // check if  there is truckscale id
      if (!info.truckscale_id) {
        throw new Error(`Please enter truckscale.`);
      }
      const ib_weight = info.ib_weight;
      const truckscale_id = info.truckscale_id;
      const transaction_id = info.transaction_id;
      const ib_timestamp = new Date().toISOString();
      const weight_type_id = weightType.rows[0].id;
      //update to db
      const update = await transactionDb.updateInboundTransaction({
        weight_type_id,
        ib_weight,
        ib_timestamp,
        truckscale_id,
        transaction_id
      });

      const count = update.rowCount;

      // #################
      // logs
      // new values
      const new_values = {
        weight_type_id,
        ib_weight,
        ib_timestamp,
        truckscale_id,
        transaction_id
      };

      const prev_values = {
        weight_type_id: null,
        ib_weight: null,
        ib_timestamp: null,
        truckscale_id: null,
        transaction_id
      };
      const logs = {
        action_type: "UPDATE OTHERS TRANSACTION - INBOUND",
        table_affected: "ts_transactions",
        new_values,
        prev_values,
        created_at: null,
        updated_at: new Date().toISOString(),
        users_id: info.users_id
      };

      await insertActivityLogss({ logs });
      // ################# logs

      const data = {
        msg: `Inserted successfully ${count} inbound transaction.`
      };

      return data;
    }
    }
  };
};

module.exports = addInbound;
