const {
  makeTransaction,
  e_addInbound,
  e_addOutbound
} = require("../../entities/transactions/app"); // entity
const transactionDb = require("../../data-access/db-layer/transactions/app"); //db
const driversDb = require("../../data-access/db-layer/drivers/app"); //db
const transactionTypeDb = require("../../data-access/db-layer/transaction-type/app"); //db
const weightDb = require("../../data-access/db-layer/weight-types/app"); // db
const posDb = require("../../data-access/db-layer/pos/app"); //db
const { insertActivityLogss } = require("../users/app");

const { transactions } = require("../../data-access/sl-layer/transactions/app");

const {
  transactionTypes
} = require("../../data-access/sl-layer/transaction-type/app");
const { weightTypes } = require("../../data-access/sl-layer/weight-types/app");
const { drivers } = require("../../data-access/sl-layer/drivers/app");
const { truckInfos } = require("../../data-access/sl-layer/truck-infos/app");

const { users } = require("../../data-access/sl-layer/users/app");

const { io } = require("../../app"); // server for socket io

// ###############
const { decrypt, encrypt } = require("../../../crypting/app");
const axios = require("axios");
const { validateAccessRights } = require("../../validator/app"); //validator

//#######################
const addNewTransaction = require("./transactions-add");
const transactionSelectAll = require("./transactions-select-all");
const addInbound = require("./transactions-inbound-add");
const addOutbound = require("./transactions-outbound-add");
const fetchPurchaseOrder = require("./transaction-fetch-SAP");
const forTransmittalNotif = require("./notification-for-transmittal");
const selectAllWarehouses = require("./select-all-warehouses");
const scanTransaction = require("./transactions-scan");
const fsqrGrpo = require("./transactions-fsqr-grpo");
const updateTransaction = require("./transactions-update");
//#######################
const forTransmittalNotifs = forTransmittalNotif({ transactionDb, io }); // emit for transmittal count

const addNewTransactions = addNewTransaction({
  transactionDb,
  makeTransaction,
  validateAccessRights,
  driversDb,
  encrypt,
  weightDb,
  insertActivityLogss,
  forTransmittalNotifs,
  posDb,
  transactions,
  transactionTypes,
  weightTypes
});

const uc_scanTransaction = scanTransaction({
  drivers,
  transactions,
  transactionTypes,
  weightTypes,
  truckInfos,
  validateAccessRights
});

const uc_selectAllWarehouses = selectAllWarehouses({ transactions });

const transactionSelectAlls = transactionSelectAll({
  transactionDb,
  decrypt,
  transactions,
  transactionTypes,
  drivers,
  truckInfos,
  weightTypes,
  validateAccessRights,
  users,
  axios
});

const uc_addInbound = addInbound({
  transactionDb,
  weightDb,
  e_addInbound,
  e_addOutbound,
  insertActivityLogss,
  transactions,
  transactionTypes,
  weightTypes,
  validateAccessRights
});
const uc_addOutbound = addOutbound({
  transactionDb,
  weightDb,
  e_addOutbound,
  insertActivityLogss,
  transactions,
  transactionTypes,
  weightTypes,
  validateAccessRights
});
const fetchPurchaseOrders = fetchPurchaseOrder({
  transactionDb,
  axios,
  transactionTypeDb,
  forTransmittalNotifs,
  transactions
});
const fsqrGrpos = fsqrGrpo({ transactions });
const updateTransactions = updateTransaction({ transactions, users });

// ###############
const services = Object.freeze({
  addNewTransactions,
  transactionSelectAlls,
  uc_addInbound,
  uc_addOutbound,
  fetchPurchaseOrders,
  forTransmittalNotifs,
  uc_selectAllWarehouses,
  uc_scanTransaction,
  fsqrGrpos,
  updateTransactions
});

module.exports = services;
module.exports = {
  addNewTransactions,
  transactionSelectAlls,
  uc_addInbound,
  uc_addOutbound,
  fetchPurchaseOrders,
  forTransmittalNotifs,
  uc_selectAllWarehouses,
  uc_scanTransaction,
  fsqrGrpos,
  updateTransactions
};

// ** NOTE
// Cannot run unit test on functions with socket.io
