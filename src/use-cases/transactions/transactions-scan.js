const scanTransaction = ({
  drivers,
  transactions,
  transactionTypes,
  weightTypes,
  truckInfos,
  validateAccessRights,
}) => {
  return async function post(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "transaction",
      "add transaction"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;

      delete info.source;
      delete info.mode;

      validateRequestBody({ info });

      if (
        info.transactionType === "delivery" ||
        info.transactionType === "others"
      ) {
        //get transaction details
        const transactionDetails = await transactions.transactionsSelectOneByTrackingCode(
          { info }
        );
        // expire session;
        if (transactionDetails.status == 401) {
          throw new Error("Session expired. Please login again.");
        } else {
          //if transaction does not exist
          if (transactionDetails.data.value.length === 0) {
            //declare variables
            let driverId,
              truckInfoId,
              transactionTypeId,
              grossWeightId,
              tareWeightId;

            //get transaction type details
            const transactionTypeDetails = await transactionTypes.transactionTypesSelectByName(
              {
                U_TS_TRNSTYPE: info.transactionType,
                cookie: info.cookie,
              }
            );

            //get transaction type id
            transactionTypeId = transactionTypeDetails.data[0].Code;

            //get gross weight details
            const grossWeightDetails = await weightTypes.weightTypesSelectByName(
              {
                U_TS_WEIGHT: "gross",
                cookie: info.cookie,
              }
            );

            //get tare weight details
            const tareWeightDetails = await weightTypes.weightTypesSelectByName(
              {
                U_TS_WEIGHT: "tare",
                cookie: info.cookie,
              }
            );

            //get gross weight Id
            grossWeightId = grossWeightDetails.data[0].Code;

            //get tare weight Id
            tareWeightId = tareWeightDetails.data[0].Code;

            //get truck info details
            const truckInfoDetails = await truckInfos.truckInfosSelectByPlateNumber(
              { info }
            );

            //check if plate number exist
            if (truckInfoDetails.data.length === 0) {
              throw new Error(
                JSON.stringify({
                  code: 1,
                  message: "Plate number does not exist",
                })
              );
            } else {
              truckInfoId = truckInfoDetails.data[0].Code;
            }

            //split driver to get firstname and lastname
            const driverDetailsFromBody = info.driver.split(", ");

            if (!driverDetailsFromBody[1] || !driverDetailsFromBody[0]) {
              throw new Error(`Invalid driver name format`);
            }

            const driverFirstName = driverDetailsFromBody[1].toLowerCase();
            const driverLastName = driverDetailsFromBody[0].toLowerCase();

            //check driver if exist or not
            const checkDriver = await drivers.driversAddSelectByName({
              info: {
                U_TS_FN: driverFirstName,
                U_TS_LN: driverLastName,
                cookie: info.cookie,
              },
            });

            if (checkDriver.data.length > 0) {
              driverId = checkDriver.data[0].Code;
            } else {
              //get Driver Max Code
              const driverMax = await drivers.driversGetMaxCode({});
              const driverMaxCode = driverMax[0].maxCode;

              //insert driver
              const insertDriver = await drivers.driversAdd({
                info: {
                  Code: driverMaxCode,
                  Name: driverMaxCode,
                  U_TS_FN: driverFirstName,
                  U_TS_LN: driverLastName,
                  cookie: info.cookie,
                },
              });

              if (insertDriver.status == 201) {
                driverId = driverMaxCode;
              } else {
                throw new Error(`Insert driver failed`);
              }
            }

            //build final object; determine if napier or not
            let deliveryTransactionDetails = {};
            if (info.isNapier) {
              // if true; means napier;
              deliveryTransactionDetails = {
                U_TS_TRNS_TYPE: transactionTypeId,
                U_TS_STATUS: "for inbound",
                U_TS_NUM_BAGS: info.no_of_bags,
                U_TS_TRCK_CODE: info.trackingCode,
                U_TS_LOC_DLVRY: info.locationDelivery,
                U_TS_IS_NAPIER: 1,
                BFI_TS_IBTRNCollection: [
                  {
                    U_TS_WEIGHT_ID: grossWeightId,
                    U_TS_CREATEDBY: info.created_by,
                    U_TS_DRIVERID: driverId,
                    U_TS_TRKINFO: truckInfoId,
                    U_TS_CREATEDATE: info.created_date,
                    U_TS_CREATETIME: info.created_time,
                  },
                ],
                BFI_TS_OBTRNCollection: [
                  {
                    U_TS_WEIGHT_ID: tareWeightId,
                    U_TS_CREATEDBY: info.created_by,
                    U_TS_DRIVERID: driverId,
                    U_TS_TRKINFO: truckInfoId,
                    U_TS_CREATEDATE: info.created_date,
                    U_TS_CREATETIME: info.created_time,
                  },
                ],
                cookie: info.cookie,
              };
            } else {
              deliveryTransactionDetails = {
                U_TS_TRNS_TYPE: transactionTypeId,
                U_TS_STATUS: "for inbound",
                U_TS_NUM_BAGS: info.no_of_bags,
                U_TS_TRCK_CODE: info.trackingCode,
                U_TS_LOC_DLVRY: info.locationDelivery,
                U_TS_IS_NAPIER: 0,
                BFI_TS_IBTRNCollection: [
                  {
                    U_TS_WEIGHT_ID: grossWeightId,
                    U_TS_CREATEDBY: info.created_by,
                    U_TS_DRIVERID: driverId,
                    U_TS_TRKINFO: truckInfoId,
                    U_TS_CREATEDATE: info.created_date,
                    U_TS_CREATETIME: info.created_time,
                  },
                ],
                BFI_TS_OBTRNCollection: [
                  {
                    U_TS_WEIGHT_ID: tareWeightId,
                    U_TS_CREATEDBY: info.created_by,
                    U_TS_DRIVERID: driverId,
                    U_TS_TRKINFO: truckInfoId,
                    U_TS_CREATEDATE: info.created_date,
                    U_TS_CREATETIME: info.created_time,
                  },
                ],
                cookie: info.cookie,
              };
            }

            const insertTransaction = await transactions.transactionsAdd(
              deliveryTransactionDetails
            );

            if (insertTransaction.status == 201) {
              return { transactionId: insertTransaction.data.DocEntry };
            } else {
              throw new Error(`Insert transaction failed`);
            }

            //if transaction does exist
          } else {
            //if transaction is completed
            if (transactionDetails.data.value[0].U_TS_STATUS === "completed") {
              if (info.specialCaseNewTransactionFlagTrue === 1) {
                //get previous data
                const prevInbound =
                  transactionDetails.data.value[0].BFI_TS_IBTRNCollection[
                    transactionDetails.data.value[0].BFI_TS_IBTRNCollection
                      .length - 1
                  ];
                const prevOutbound =
                  transactionDetails.data.value[0].BFI_TS_OBTRNCollection[
                    transactionDetails.data.value[0].BFI_TS_OBTRNCollection
                      .length - 1
                  ];

                //build final object
                const deliverySpecialCaseTransactionDetails = {
                  id: transactionDetails.data.value[0].DocEntry,
                  U_TS_STATUS: "for outbound",
                  BFI_TS_IBTRNCollection: [
                    {
                      U_TS_WEIGHT_ID: prevInbound.U_TS_WEIGHT_ID,
                      U_TS_CREATEDBY: prevInbound.U_TS_CREATEDBY,
                      U_TS_DRIVERID: prevInbound.U_TS_DRIVERID,
                      U_TS_IB_WEIGHT: prevOutbound.U_TS_OB_WEIGHT,
                      U_TS_TRKINFO: prevInbound.U_TS_TRKINFO,
                      U_TS_CREATEDATE: prevInbound.U_TS_CREATEDATE,
                      U_TS_CREATETIME: prevInbound.U_TS_CREATETIME,
                      U_TS_UPDATEDATE: prevInbound.U_TS_CREATEDATE,
                      U_TS_UPDATETIME: prevInbound.U_TS_CREATETIME,
                    },
                  ],
                  BFI_TS_OBTRNCollection: [
                    {
                      U_TS_WEIGHT_ID: prevOutbound.U_TS_WEIGHT_ID,
                      U_TS_CREATEDBY: prevOutbound.U_TS_CREATEDBY,
                      U_TS_DRIVERID: prevOutbound.U_TS_DRIVERID,
                      U_TS_TRKINFO: prevOutbound.U_TS_TRKINFO,
                      U_TS_CREATEDATE: prevOutbound.U_TS_CREATEDATE,
                      U_TS_CREATETIME: prevOutbound.U_TS_CREATETIME,
                    },
                  ],
                  cookie: info.cookie,
                };

                const updateTransaction = await transactions.transactionsUpdate(
                  deliverySpecialCaseTransactionDetails
                );

                return {
                  transactionId: transactionDetails.data.value[0].DocEntry,
                };
              } else {
                throw new Error(
                  JSON.stringify({
                    code: 2,
                    message: "Transaction already completed",
                    isNapier: transactionDetails.data.value[0].U_TS_IS_NAPIER,
                  })
                );
              }
            } else {
              let isNapier = 0;
              if (info.isNapier == true) {
                isNapier = 1;
              }

              if (
                transactionDetails.data.value[0].U_TS_IS_NAPIER !== isNapier
              ) {
                throw new Error(
                  JSON.stringify({
                    code: 3,
                    message: "Not allowed to transact here.",
                  })
                );
              }

              return {
                transactionId: transactionDetails.data.value[0].DocEntry,
              };
            }
          }
        }
      }

      return info;
    } else {
      // offline mode
    }
  };
};

const validateRequestBody = ({ info }) => {
  const {} = info;
};

module.exports = scanTransaction;
