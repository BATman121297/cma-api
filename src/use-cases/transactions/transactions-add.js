const addNewTransaction = ({
  transactionDb,
  makeTransaction,
  validateAccessRights,
  driversDb,
  encrypt,
  weightDb,
  insertActivityLogss,
  forTransmittalNotifs,
  posDb,
  transactions,
  transactionTypes,
  weightTypes
}) => {
  return async function posts(info) {
    const mode = info.mode

    if(!info.modules){
      throw new Error (`Access denied`)
    }

    const allowed = await validateAccessRights(info.modules,'transaction','add transaction')


    if(!allowed){
      throw new Error (`Access denied`)
    }


    //Online Mode
    if(mode == 1){

      
      delete info.modules

      delete info.source;
      delete info.mode;

      //todo validation
      if(!info.t_type){
        throw new Error(`Please input transaction type.`)
      }

      //Prereq
      const transactionTypeDetails = await transactionTypes.transactionTypesSelectByName({
        U_TS_TRNSTYPE: info.t_type,
        cookie: info.cookie
      })

      const grossWeightDetails = await weightTypes.weightTypesSelectByName({
        U_TS_WEIGHT: 'gross',
        cookie: info.cookie
      })

      const tareWeightDetails = await weightTypes.weightTypesSelectByName({
        U_TS_WEIGHT: 'tare',
        cookie: info.cookie
      })

      const transactionTypeId = transactionTypeDetails.data[0].Code
      const grossWeightId = grossWeightDetails.data[0].Code
      const tareWeightId = tareWeightDetails.data[0].Code
      //Prereq


      if(info.t_type === 'others'){


        //build object for sap
        const transactionDetails = {
            U_TS_TRNS_TYPE: transactionTypeId,
            U_TS_STATUS: "others",
            BFI_TS_IBTRNCollection: [
                {
                    U_TS_WEIGHT_ID: grossWeightId,
                    U_TS_CREATEDBY: info.createdby,
                    U_TS_TRKSCL_ID: info.truckscale_id,
                    U_TS_DRIVERID: info.driver_id,
                    U_TS_TRKINFO: info.truck_info_id,
                    U_TS_CREATEDATE: info.created_date,
                    U_TS_CREATETIME: info.created_time
                }
            ],
            BFI_TS_OBTRNCollection: [
                {
                    U_TS_WEIGHT_ID: tareWeightId,
                    U_TS_CREATEDBY: info.createdby,
                    U_TS_TRKSCL_ID: info.truckscale_id,
                    U_TS_DRIVERID: info.driver_id,
                    U_TS_TRKINFO: info.truck_info_id,
                    U_TS_CREATEDATE: info.created_date,
                    U_TS_CREATETIME: info.created_time
                }
            ],
            cookie: info.cookie 
        }


        //insertion
        const res = await transactions.transactionsAdd( transactionDetails );

        const reqStatus = res.status;
        if (reqStatus == 201) {
          return res;
        } else {
          // throw error from SL
          throw new Error(res.data.error);
        }
      }else if(info.t_type === 'withdrawal'){

        //get transaction details

        const transactionDetails = await transactions.transactionsSelectOne({info})

        if(transactionDetails.status === 404){
          throw new Error(`Transaction does not exist.`)
        }
  
        const transactionId = transactionDetails.data.DocEntry


        //build object for sap
        const transactionWithdrawalDetails = {
            id: transactionId,
            U_TS_TRNS_TYPE: transactionTypeId,
            U_TS_STATUS: "for inbound",
            U_TS_LOC_DLVRY: info.location_delivery,
            U_APP_WHSCODE: info.warehouse_code,
            U_TS_TRNSMTTL_NO: info.transmittal_number,
            BFI_TS_IBTRNCollection: [
                {
                    U_TS_WEIGHT_ID: tareWeightId,
                    U_TS_CREATEDBY: info.createdby,
                    U_TS_DRIVERID: info.driver_id,
                    U_TS_TRKINFO: info.truck_info_id,
                    U_TS_CREATEDATE: info.created_date,
                    U_TS_CREATETIME: info.created_time
                }
            ],
            BFI_TS_OBTRNCollection: [
                {
                    U_TS_WEIGHT_ID: grossWeightId,
                    U_TS_CREATEDBY: info.createdby,
                    U_TS_DRIVERID: info.driver_id,
                    U_TS_TRKINFO: info.truck_info_id,
                    U_TS_CREATEDATE: info.created_date,
                    U_TS_CREATETIME: info.created_time
                }
            ],
            cookie: info.cookie 
        }

        // update to withdrawal transaction
        const res = await transactions.transactionsUpdate( transactionWithdrawalDetails );
        return res;


      }else if(info.t_type === 'delivery'){
        return info
      }

      


    }else {

    const result = makeTransaction(info);
    let transactionTypeId; // hold transaction type id
    let supplierId; // hold supplier id
    let rawMaterialId; // hold raw material id
    let driverId; // hold driver id



    // get transaction type id
    const transactionType = await transactionDb.selectOneTransactionType({
      transaction_type: result.getTransactionType()
    });
    // transaction type id
    transactionTypeId = transactionType.rows[0].id;

    // // get supplier id
    // const supplier = await transactionDb.selectOneSupplier({
    //   supplier: result.getSupplier()
    // });

    // // meaning if supplier exist
    // if (supplier.rowCount > 0) {
    //   // supplier id
    //   supplierId = supplier.rows[0].id;
    // } else {
    //   // insert supplier; then return id
    //   const insert = await transactionDb.insertSupplier({
    //     supplier: result.getSupplier()
    //   });
    //   // supplier id
    //   supplierId = insert.rows[0].id;
    // }

    // get raw material id
    // const rawMaterial = await transactionDb.selectOneRawMaterial({
    //   raw_material_name: result.getRawMaterial()
    // });

    // if (rawMaterial.rowCount > 0) {
    //   // raw material id
    //   rawMaterialId = rawMaterial.rows[0].id;
    // } else {
    //   // insert raw material; then return id
    //   const insert = await transactionDb.insertRawMaterial({
    //     raw_material_name: result.getRawMaterial(),
    //     created_by: result.getCreatedBy(),
    //     created_at: result.getCreatedAt()
    //   });
    //   // raw material id
    //   rawMaterialId = insert.rows[0].id;
    // }

    // transaction type
    const tType = result.getTransactionType();
    const tTypeLower = tType.toLowerCase();

    

    // transaction type is delivery
    if (tTypeLower === "delivery") {

      

      // ##########################
      // Auto insert driver
      // auto select / insert driver
      if (!info.driver) {
        throw new Error(`Please enter driver's information.`);
      }
      if (!info.driver.firstname) {
        throw new Error(`Please enter driver's first name.`);
      }
      if (!info.driver.lastname) {
        throw new Error(`Please enter driver's last name.`);
      }
      // get driver id
      const driverExist = await driversDb.findByName({
        firstname: encrypt(info.driver.firstname),
        lastname: encrypt(info.driver.lastname)
      });

      if (driverExist.rowCount > 0) {
        driverId = driverExist.rows[0].id; // driver's id
      } else {
        // insert raw material; then return id
        const insert = await driversDb.insertDriverInTransaction({
          firstname: encrypt(info.driver.firstname),
          lastname: encrypt(info.driver.lastname),
          created_at: new Date().toISOString()
        });
        // raw material id
        driverId = insert.rows[0].id;
      }
      // ########################## end here

      // check if tracking code exist already; return transaction id
      const trackCode = result.getTrackingCode();
      const code = await transactionDb.checkTrackingCode({ code: trackCode });
      if (code.rowCount > 0) {
        const transactionInfo = await transactionDb.selectOneTransactionById({
          id: code.rows[0].id
        });

        // status of the transaction
        const transactionStatus = transactionInfo.rows[0].status.toLowerCase();

        // check status
        if (transactionStatus === "completed") {
          // to do
          const data = {
            msg: `re-scan QR; for outbound - completed`,
            transactionId: code.rows[0].id,
            hasOutbound: true
          };

          return data;
        }

        if (transactionStatus === "for inbound") {
          // re-scan just return QR
          const data = {
            msg: `re-scan QR`,
            transactionId: code.rows[0].id,
            hasInbound: true
          };

          return data;
        }

        if (transactionStatus === "for outbound") {
          if (!info.plate_number) {
            throw new Error(`Please enter plate number.`);
          }
          const plateNumberExist = await transactionDb.selectTruckTypeInfo({
            plate_number: info.plate_number
          });
          if (plateNumberExist.rowCount > 0) {
            // truck infoid
            const truckInfoId = plateNumberExist.rows[0].id;

            const weightType = await weightDb.selectByName({
              weight_name: "Tare"
            }); //set weight type

            const weight_type_id = weightType.rows[0].id;

            // delete first before insert; to avoid duplicates
            await transactionDb.deleteNullOutboundTransactions({
              id: code.rows[0].id
            });

            // insert to outbound
            const insert = await transactionDb.addOutboundTransaction({
              transaction_id: code.rows[0].id,
              weight_type_id,
              users_id: info.users_id,
              truckscale_id: info.truckscale_id,
              drivers_id: driverId,
              truck_info_id: truckInfoId
            });

            // #################
            // logs
            // new values
            const new_values = {
              transaction_id: code.rows[0].id,
              weight_type_id,
              users_id: info.users_id,
              truckscale_id: info.truckscale_id,
              drivers_id: driverId,
              truck_info_id: truckInfoId
            };

            const logs = {
              action_type: "CREATE DELIVERY TRANSACTION - OUTBOUND",
              table_affected: "ts_outbound_transactions",
              new_values,
              prev_values: null,
              created_at: new Date().toISOString(),
              updated_at: null,
              users_id: info.users_id
            };

            await insertActivityLogss({ logs });
            // ################# logs

            const data = {
              msg: `re-scan QR; for outbound`,
              transactionId: code.rows[0].id,
              hasOutbound: true
            };

            return data;
          } else {
            // if doesn't exist
            const data = {
              msg: `Failed to add transaction; for outbound.`,
              hasOutbound: false
            };
            return data;
          }
        }
      } else {
        if (!info.plate_number) {
          throw new Error(`Please enter plate number.`);
        }

        // check plate number
        const plateNumberExist = await transactionDb.selectTruckTypeInfo({
          plate_number: info.plate_number
        });

        // if exist plate number
        if (plateNumberExist.rowCount > 0) {
          // if code doesn't exist
          // insert to db
          
          

          let poData = await posDb.selectPoByNumber(result.getPurchaseOrderId())

          if(poData.rowCount === 0){
            throw new Error(`PO does not exist in SAP`)
          }

          let poId = poData.rows[0].id

          const insert = await transactionDb.insertNewTransaction({
            purchase_order_id: poId,
            transaction_type_id: transactionTypeId,
            status: "for inbound",
            created_at: result.getCreatedAt(),
            created_by: result.getCreatedBy(),
            tracking_code: result.getTrackingCode(),
            no_of_bags: result.getNumberOfBags()
          });

          // #################
          // logs
          // new values
          const new_values = {
            // supplier_id: supplierId,
            // raw_material_id: rawMaterialId,
            transaction_type_id: transactionTypeId,
            status: "for inbound",
            created_at: result.getCreatedAt(),
            created_by: result.getCreatedBy(),
            tracking_code: result.getTrackingCode(),
            no_of_bags: result.getNumberOfBags()
          };

          const logs = {
            action_type: "CREATE DELIVERY TRANSACTION",
            table_affected: "ts_transactions",
            new_values,
            prev_values: null,
            created_at: new Date().toISOString(),
            updated_at: null,
            users_id: result.getCreatedBy()
          };

          await insertActivityLogss({ logs });
          // ################# logs

          // truck infoid
          const truckInfoId = plateNumberExist.rows[0].id;
          const weightType = await weightDb.selectByName({
            weight_name: "Gross"
          }); //set weight type

          // delete first before insert; to avoid duplicates
          await transactionDb.deleteNullInboundTransactions({
            id: insert.rows[0].id
          });

          // insert to inbound
          const inbound = await transactionDb.addInboundTransaction({
            transaction_id: insert.rows[0].id,
            weight_type_id: weightType.rows[0].id,
            users_id: info.users_id,
            truckscale_id: info.truckscale_id,
            drivers_id: driverId,
            truck_info_id: truckInfoId
          });

          const data = {
            msg: `for inbound`,
            transactionId: insert.rows[0].id,
            hasInbound: true
          };

          // #################
          // logs
          // new values
          const new_valuess = {
            transaction_id: insert.rows[0].id,
            weight_type_id: weightType.rows[0].id,
            users_id: info.users_id,
            truckscale_id: info.truckscale_id,
            drivers_id: driverId,
            truck_info_id: truckInfoId
          };

          const logss = {
            action_type: "CREATE DELIVERY TRANSACTION - INBOUND",
            table_affected: "ts_inbound_transactions",
            new_values: new_valuess,
            prev_values: null,
            created_at: new Date().toISOString(),
            updated_at: null,
            users_id: info.users_id
          };

          await insertActivityLogss({ logs: logss });
          // ################# logs

          return data;
        } else {
          // if doesn't exist
          const data = {
            msg: `Failed to add transaction.`,
            hasInbound: false
          };
          return data;
        }
      }
    }
    // end delivery

    // ####################
    // transaction type is others
    if (tTypeLower === "others") {
      // check if there is drivers id in the req body
      if (!info.drivers_id) {
        throw new Error(`Please enter driver.`);
      }
      // check truck info is required
      if (!info.truck_info_id) {
        throw new Error("Please enter truck info.");
      }
      // insert to db
      const insert = await transactionDb.insertNewTransactionOthers({
        // supplier_id: supplierId,
        // raw_material_id: rawMaterialId,
        transaction_type_id: transactionTypeId,
        status: "others",
        created_at: result.getCreatedAt(),
        created_by: result.getCreatedBy(),
        drivers_id: info.drivers_id,
        truck_info_id: info.truck_info_id
      });

      // #################
      // logs
      // new values
      const new_values = {
        // supplier_id: supplierId,
        // raw_material_id: rawMaterialId,
        transaction_type_id: transactionTypeId,
        status: "others",
        created_at: result.getCreatedAt(),
        created_by: result.getCreatedBy(),
        drivers_id: info.drivers_id,
        truck_info_id: info.truck_info_id
      };

      const logs = {
        action_type: "CREATE OTHERS TRANSACTION",
        table_affected: "ts_transactions",
        new_values,
        prev_values: null,
        created_at: new Date().toISOString(),
        updated_at: null,
        users_id: result.getCreatedBy()
      };

      await insertActivityLogss({ logs });
      // ################# logs

      const data = {
        msg: `Inserted successfully transaction.`
      };

      return data;
    }
    // end others

    // ####################
    // transaction type is withdrawal
    // wait for API of SAP; make a route for worker
    if (tTypeLower === "withdrawal") {
      // if it doesn't exist; UI or QR code can insert
      // check PO if exist
   
      const poExist = await transactionDb.checkPurchaseOrderId({
        purchase_order_id: result.getPurchaseOrderId()
      });

      if (poExist.rowCount > 0) {
        // get status of transaction

        const transactionId = poExist.rows[0].id;

        const transactionInfo = await transactionDb.selectOneTransactionById({
          id: transactionId
        });
        // status of transaction
        const transactionStatus = transactionInfo.rows[0].status.toLowerCase();

        if (transactionStatus === "for transmittal") {
          // update db; from info sheet
          const insert = await transactionDb.updateWithdrawalTransaction({
            // supplier_id: supplierId,
            // raw_material_id: rawMaterialId,
            transaction_type_id: transactionTypeId,
            status: "for inbound",
            updated_at: result.getUpdatedAt(),
            location_delivery: result.getLocationDelivery(),
            warehouse_location: result.getWarehouseLocations(),
            no_of_bags: result.getNumberOfBags(),
            transmittal_number: result.getTransmittalNumber(),
            created_by: result.getCreatedBy(),
            id: transactionId
          });

          // emit notif
          await forTransmittalNotifs({});

          // #################
          // logs
          // new values
          const new_values = {
            // supplier_id: supplierId,
            // raw_material_id: rawMaterialId,
            transaction_type_id: transactionTypeId,
            status: "for inbound",
            updated_at: result.getUpdatedAt(),
            location_delivery: result.getLocationDelivery(),
            warehouse_location: result.getWarehouseLocations(),
            no_of_bags: result.getNumberOfBags(),
            transmittal_number: result.getTransmittalNumber(),
            created_by: result.getCreatedBy(),
            id: transactionId
          };

          const prev_values = {
            id: transactionId,
            purchaseOrder_id: result.getPurchaseOrderId()
          };

          const logs = {
            action_type: "UPDATE FOR TRANSMITTAL TRANSACTION",
            table_affected: "ts_transactions",
            new_values,
            prev_values,
            created_at: new Date().toISOString(),
            updated_at: null,
            users_id: result.getCreatedBy()
          };

          await insertActivityLogss({ logs });
          // ################# logs

          // return updated msg
          const count = insert.rowCount; // count how many updated data
          const data = {
            msg: `Updated successfully ${count} transaction.`
          };
          return data;
        } else {
          // select data; from QR

          // to insert auto inbound transaction

          // ##########################

          // Auto insert driver
          // auto select / insert driver
          if (!info.driver) {
            throw new Error(`Please enter driver's information.`);
          }
          if (!info.driver.firstname) {
            throw new Error(`Please enter driver's first name.`);
          }
          if (!info.driver.lastname) {
            throw new Error(`Please enter driver's last name.`);
          }
          // get driver id
          const driverExist = await driversDb.findByName({
            firstname: encrypt(info.driver.firstname),
            lastname: encrypt(info.driver.lastname)
          });

          if (driverExist.rowCount > 0) {
            driverId = driverExist.rows[0].id; // driver's id
          } else {
            // insert raw material; then return id
            const insert = await driversDb.insertDriverInTransaction({
              firstname: encrypt(info.driver.firstname),
              lastname: encrypt(info.driver.lastname),
              created_at: new Date().toISOString()
            });
            // raw material id
            driverId = insert.rows[0].id;
          }

          const plateNumberExist = await transactionDb.selectTruckTypeInfo({
            plate_number: info.plate_number
          });

          // if exist plate number
          if (plateNumberExist.rowCount > 0) {
            // truck infoid
            const truckInfoId = plateNumberExist.rows[0].id;

            if (transactionStatus === "for inbound") {
              const weightType = await weightDb.selectByName({
                weight_name: "Tare"
              }); //set weight type

              // delete first before insert; to avoid duplicates
              await transactionDb.deleteNullInboundTransactions({
                id: transactionId
              });

              // insert to inbound
              await transactionDb.addInboundTransaction({
                transaction_id: transactionId,
                weight_type_id: weightType.rows[0].id,
                users_id: info.users_id,
                truckscale_id: info.truckscale_id,
                drivers_id: driverId,
                truck_info_id: truckInfoId
              });

              // #################
              // logs
              // new values
              const new_values = {
                transaction_id: transactionId,
                weight_type_id: weightType.rows[0].id,
                users_id: info.users_id,
                truckscale_id: info.truckscale_id,
                drivers_id: driverId,
                truck_info_id: truckInfoId
              };

              const logs = {
                action_type: "CREATE WITHDRAWAL TRANSACTION - INBOUND",
                table_affected: "ts_transactions",
                new_values,
                prev_values: null,
                created_at: new Date().toISOString(),
                updated_at: null,
                users_id: info.users_id
              };

              await insertActivityLogss({ logs });
              // ################# logs
            }

            if (transactionStatus === "for outbound") {
              const weightType = await weightDb.selectByName({
                weight_name: "Gross"
              }); //set weight type

              const weight_type_id = weightType.rows[0].id;

              // delete first before insert; to avoid duplicates
              await transactionDb.deleteNullOutboundTransactions({
                id: transactionId
              });

              // insert to outbound
              await transactionDb.addOutboundTransaction({
                transaction_id: transactionId,
                weight_type_id,
                users_id: info.users_id,
                truckscale_id: info.truckscale_id,
                drivers_id: driverId,
                truck_info_id: truckInfoId
              });

              // #################
              // logs
              // new values
              const new_values = {
                transaction_id: transactionId,
                weight_type_id,
                users_id: info.users_id,
                truckscale_id: info.truckscale_id,
                drivers_id: driverId,
                truck_info_id: truckInfoId
              };

              const logs = {
                action_type: "CREATE WITHDRAWAL TRANSACTION - OUTBOUND",
                table_affected: "ts_transactions",
                new_values,
                prev_values: null,
                created_at: new Date().toISOString(),
                updated_at: null,
                users_id: info.users_id
              };

              await insertActivityLogss({ logs });
              // ################# logs
            }
          }
          // end auto insert inbound transaction

          // return
          const data = {
            msg: `Transaction exist..`,
            transactionId
          };

          return data;
        }
      } else {
        // return QR; to be inserted manually; modal
   
        return info;
      }
    }
  }
  };
};

module.exports = addNewTransaction;
