const {
  makeActions,
  makeActionsUpdate
} = require("../../entities/actions/app"); // entity
const actionsDb = require("../../data-access/db-layer/actions/app"); //db
const { decrypt } = require("../../../crypting/app");
const { insertActivityLogss } = require("../users/app"); // logs

const { actions } = require("../../data-access/sl-layer/actions/app");

const { objectLowerCaser } = require("../../lowerCaser/app"); 

const { validateAccessRights } = require("../../validator/app")

//####################
const addNewAction = require("./actions-add");
const updateActions = require("./actions-update");
const actionsSelectAll = require("./actions-select-all");
const actionsSelectOne = require("./actions-select-one");

//####################
const addNewActions = addNewAction({
  actionsDb,
  makeActions,
  insertActivityLogss,
  decrypt,
  actions,
  objectLowerCaser,
  validateAccessRights
});
const updateActionss = updateActions({
  actionsDb,
  makeActionsUpdate,
  insertActivityLogss,
  decrypt,
  actions,
  objectLowerCaser,
  validateAccessRights
});
const actionsSelectAlls = actionsSelectAll({ actionsDb, decrypt, actions, validateAccessRights });
const actionsSelectOnes = actionsSelectOne({ actionsDb, decrypt, actions, validateAccessRights });

const services = Object.freeze({
  addNewActions,
  updateActionss,
  actionsSelectAlls,
  actionsSelectOnes
});

module.exports = services;
module.exports = {
  addNewActions,
  updateActionss,
  actionsSelectAlls,
  actionsSelectOnes
};
