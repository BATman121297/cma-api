const addTruckInfo = ({
  truckInfosDb,
  truckTypesDb,
  e_addTruckInfo,
  insertActivityLogss,
  truckInfos,
  objectLowerCaser,
  validateAccessRights
}) => {
  return async function post(info) {

    const mode = info.mode

    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    const allowed = await validateAccessRights(info.modules, 'truck and driver', 'add truck')


    if (!allowed) {
      throw new Error(`Access denied`)
    }

    if (mode == 1) {


      delete info.modules

      delete info.source;
      delete info.mode;


      const max = await truckInfos.truckInfosGetMaxCode({})
      const maxCode = max[0].maxCode;

      info = await objectLowerCaser(info)
      val({ info })



      info.Code = maxCode;
      info.Name = maxCode;


      const check = await truckInfos.truckInfosAddSelectByName({ info })

      const length = check.data.length;

      if (length > 0) {
        throw new Error(`Plate number already exists.`);
      }

      const res = await truckInfos.truckInfosAdd({ info });

      const reqStatus = res.status;
      if (reqStatus == 201) {
        return res;
      } else {
        // throw error from SL
        throw new Error(res.data.error.message.value);
      }

    } else {


      const entity = await e_addTruckInfo(info);

      const truckTypeExist = await truckTypesDb.selectOneTruckType({
        id: entity.getTruckTypeId()
      });

      if (truckTypeExist.rowCount === 0) {
        throw new Error(`Truck type doesn't exist.`);
      }

      const plateNumberExists = await truckInfosDb.selectOneTruckInfoByPlateNumber(
        { plate_number: entity.getPlateNumber() }
      );

      if (plateNumberExists.rowCount !== 0) {
        throw new Error(`Plate number already exists.`);
      }

      const insert = await truckInfosDb.insertTruckInfo({
        truck_type_id: entity.getTruckTypeId(),
        plate_number: entity.getPlateNumber(),
        created_at: entity.getCreatedAt()
      });

      const count = insert.rowCount;

      const data = {
        msg: `Inserted successfully ${count} truck info.`
      };

      // logs
      // new values
      const new_values = {
        truck_type_id: entity.getTruckTypeId(),
        plate_number: entity.getPlateNumber(),
        created_at: entity.getCreatedAt()
      };

      const logs = {
        action_type: "CREATE TRUCK INFO",
        table_affected: "ts_truck_infos",
        new_values,
        prev_values: null,
        created_at: new Date().toISOString(),
        updated_at: null,
        users_id: info.users_id
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};

const val = ({ info }) => {
  const { U_TS_TRTYPE_ID, U_TS_PLATE_NUM } = info;

  if (!U_TS_TRTYPE_ID) {
    const d = {
      msg: "Please enter truck model."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_PLATE_NUM) {
    const d = {
      msg: "Please enter plate number."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = addTruckInfo;
