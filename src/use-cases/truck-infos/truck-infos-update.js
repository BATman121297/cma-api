const updateTruckInfo = ({
  truckInfosDb,
  truckTypesDb,
  e_updateTruckInfo,
  insertActivityLogss,
  truckInfos,
  objectLowerCaser,
  validateAccessRights
}) => {
  return async function put({ id, ...info } = {}) {

    const mode = info.mode

    if(!info.modules){
      throw new Error (`Access denied`)
    }

    const allowed = await validateAccessRights(info.modules,'truck and driver','edit truck')


    if(!allowed){
      throw new Error (`Access denied`)
    }

    if (mode == 1){
      
      delete info.modules

      delete info.mode; 
      delete info.source;


      info.id = id

      
      info = await objectLowerCaser(info)

      val({info})


      const check = await truckInfos.truckInfosAddSelectByName({info})

      const length = check.data.length;

      if (length > 0) {
        throw new Error(`Plate number already exists.`);
      }


      const res = await truckInfos.truckInfosUpdate({ info });
      return res;

    }else {


    const entity = await e_updateTruckInfo(info);
    const exist = await truckInfosDb.selectOneTruckInfo({ id });
    
    if (exist.rowCount === 0) {
      throw new Error(`Truck info doesn't exist.`);
    }

    const truckTypeExist = await truckTypesDb.selectOneTruckType({
      id: entity.getTruckTypeId()
    });

    if (truckTypeExist.rowCount === 0) {
      throw new Error(`Truck type doesn't exist.`);
    }

    const plateNumberExists = await truckInfosDb.selectOneTruckInfoByPlateNumberUpdate(
      { plate_number: entity.getPlateNumber(), id }
    );

    if (plateNumberExists.rowCount !== 0) {
      throw new Error(`Plate number already exists.`);
    }

    // query previous values
    const previous = await truckInfosDb.selectOneTruckInfo({ id });
    const prev_data = previous.rows[0];
    const prev_values = {
      truckinfo_id: prev_data.truckinfo_id,
      plate_number: prev_data.plate_number,
      trucktype_id: prev_data.trucktype_id,
      truck_type: prev_data.truck_type,
      truck_model: prev_data.truck_model,
      truck_size: prev_data.truck_size
    };

    // update
    const update = await truckInfosDb.updateTruckInfo({
      truck_type_id: entity.getTruckTypeId(),
      plate_number: entity.getPlateNumber(),
      updated_at: entity.getUpdatedAt(),
      id
    });

    const count = update.rowCount;

    const data = {
      msg: `Updated successfully ${count} truck info.`
    };

    // logs
    // new values
    const new_values = {
      truck_type_id: entity.getTruckTypeId(),
      plate_number: entity.getPlateNumber(),
      updated_at: entity.getUpdatedAt()
    };

    const logs = {
      action_type: "UPDATE TRUCK INFO",
      table_affected: "ts_truck_infos",
      new_values,
      prev_values,
      created_at: null,
      updated_at: new Date().toISOString(),
      users_id: info.users_id
    };

    await insertActivityLogss({ logs });

    return data;
  }
  };
};

const val = ({ info }) => {
  const { U_TS_TRTYPE_ID, U_TS_PLATE_NUM } = info;
  
  if (!U_TS_TRTYPE_ID) {
    const d = {
      msg: "Please enter truck type ID."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_PLATE_NUM) {
    const d = {
      msg: "Please enter plate number."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = updateTruckInfo;
