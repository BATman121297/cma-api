const selectAllTruckInfos = ({ truckInfosDb, truckInfos, validateAccessRights }) => {
  return async function get(info) {

    const mode = info.mode

    if(!info.modules){
      throw new Error (`Access denied`)
    }

    const allowed = await validateAccessRights(info.modules,'truck and driver','view trucks')


    if(!allowed){
      throw new Error (`Access denied`)
    }


    if(mode == 1){
      
      delete info.modules
      delete info.source;
      delete info.mode;


      const res = await truckInfos.truckInfosSelectAll({info});

      return res


    }else {
    const select = await truckInfosDb.selectAllTruckInfos();

    const view = select.rows;
    let data = [];

    for await (let e of view) {
      let truckType = [
        {
          id: e.trucktype_id,
          truck_type: e.truck_type,
          truck_model: e.truck_model,
          truck_size: e.truck_size
        }
      ];

      data.push({
        truckinfo_id: e.truckinfo_id,
        plate_number: e.plate_number,
        truckType
      });
    }

    return data;
  }
  };
};

module.exports = selectAllTruckInfos;
