const weightTypeSelectOne = ({ weightDb }) => {
  return async function selects({ id }) {
    const result = await weightDb.selectOneWeightType({ id });
    const weightType = result.rows;

    return weightType;
  };
};

module.exports = weightTypeSelectOne;
