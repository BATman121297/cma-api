const { makeWeight } = require("../../entities/weight-types/app"); // entity
const weightDb = require("../../data-access/db-layer/weight-types/app"); //db

const { insertActivityLogss } = require("../users/app");


const { weightTypes } = require("../../data-access/sl-layer/weight-types/app")
//#######################
const addNewWeightType = require("./weight-type-add");
const weightTypeSelectAll = require("./weight-type-select-all");
const weightTypeSelectOne = require("./weight-type-select-one");
const updateWeightTypes = require("./weight-type-update");
//#######################
const addNewWeightTypes = addNewWeightType({
  weightDb,
  makeWeight,
  insertActivityLogss
});
const weightTypeSelectAlls = weightTypeSelectAll({ weightDb, weightTypes });
const weightTypeSelectOnes = weightTypeSelectOne({ weightDb });
const updateWeightTypess = updateWeightTypes({
  weightDb,
  makeWeight,
  insertActivityLogss
});

const services = Object.freeze({
  addNewWeightTypes,
  weightTypeSelectAlls,
  weightTypeSelectOnes,
  updateWeightTypess
});

module.exports = services;
module.exports = {
  addNewWeightTypes,
  weightTypeSelectAlls,
  weightTypeSelectOnes,
  updateWeightTypess
};
