const addNewModule = ({
  modulesDb,
  makeModule,
  insertActivityLogss,
  decrypt,
  modules,
  objectLowerCaser,
  validateAccessRights
}) => {
  return async function posts(info) {
    // mode; online or offline; 403 if no mode in req body
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "admin",
      "add module"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      // remove source & mode; error in submit

      delete info.modules;
      delete info.source;
      delete info.mode;

      // get max code; for auto increment
      const max = await modules.modulesGetMaxCode({ info });
      const maxCode = max[0].maxCode;

      await objectLowerCaser(info);

      // select if module name exist
      const check = await modules.modulesAddSelectByName({ info });

      if (check.status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }

      // number of data
      const length = check.length;
      // if exist
      if (length > 0) {
        const d = {
          msg: "Module name already exists."
        };
        throw new Error(JSON.stringify(d));
      }

      // insert here
      info.Code = maxCode;
      info.Name = maxCode;

      // validate
      val({ info });

      const res = await modules.modulesAdd({ info });

      const reqStatus = res.status;
      if (reqStatus == 201) {
        return res;
      } else {
        // throw error from SL
        throw new Error(JSON.stringify(res));
      }
    } else {
      const result = makeModule(info);

      const moduleExist = await modulesDb.selectModule({
        description: result.getDesc()
      });

      if (moduleExist.rowCount !== 0) {
        throw new Error("The module name already exist.");
      }

      // insert to db
      const insert = await modulesDb.insertModule({
        description: result.getDesc(),
        status: result.getStatus(),
        created_at: result.getCreatedAt(),
        created_by: result.getCreatedBy()
      });

      const count = insert.rowCount; // get the number of inserted data

      const data = {
        msg: `Inserted successfully ${count} module.`
      };

      // logs
      const user = await modulesDb.returnCreatedBy({
        id: result.getCreatedBy()
      });
      const create = user.rows[0];
      const created_by = {
        id: create.id,
        employee_id: create.employee_id ? decrypt(create.employee_id) : "",
        firstname: create.firstname ? decrypt(create.firstname) : "",
        lastname: create.lastname ? decrypt(create.lastname) : ""
      };

      // logs
      // new values
      const new_values = {
        description: result.getDesc(),
        status: result.getStatus(),
        created_at: result.getCreatedAt(),
        created_by
      };

      const logs = {
        action_type: "CREATE MODULE",
        table_affected: "ts_modules",
        new_values,
        prev_values: null,
        created_at: new Date().toISOString(),
        updated_at: null,
        users_id: info.users_id
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};

// SL validation
const val = ({ info }) => {
  const { U_TS_DESC, U_TS_CREATEDBY, U_TS_STATUS } = info;
  if (!U_TS_DESC) {
    const d = {
      msg: "Please enter module name."
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_CREATEDBY) {
    const d = {
      msg: "Please enter who created the module."
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_STATUS) {
    const d = {
      msg: "Please enter status."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = addNewModule;
