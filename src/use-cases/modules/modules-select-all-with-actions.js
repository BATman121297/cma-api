const modulesSelectAllWithActions = ({
  modulesDb,
  modules,
  validateAccessRights
}) => {
  return async function selects(info) {
    const { mode } = info;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "admin",
      "view modules"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;
      delete info.source;

      const res = await modules.modulesSelectWithActions({ info });

      //   empty arrays to store modules and actions
      let mod = [];
      let act = [];

      const status = res.status;
      if (status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }
      for await (let i of res) {
        mod.push({
          moduleid: i.U_BFI_TS_MOD.Code,
          moduledescription: i.U_BFI_TS_MOD.U_TS_DESC,
          modulestatus: i.U_BFI_TS_MOD.U_TS_STATUS
        });

        act.push({
          actionid: i.U_BFI_TS_ACT.Code,
          actiondescription: i.U_BFI_TS_ACT.U_TS_DESC,
          actionstatus: i.U_BFI_TS_ACT.U_TS_STATUS,
          actionmoduleid: i.U_BFI_TS_ACT.U_TS_MOD_ID
        });
      }

      const filteredModules = await filterArrayNoDuplicates(mod, "moduleid");

      const filteredActions = await filterArrayNoDuplicates(act, "actionid");

      for await (let i of filteredModules) {
        let temp = [];
        for await (let x of filteredActions) {
          if (i.moduleid === x.actionmoduleid) {
            temp.push(x);
          }
        }

        i.actions = temp;
      }

      return filteredModules;
    } else {
      // offline mode
      const result = await modulesDb.selectAllModulesWithActions();

      let modules = [];

      let actions = [];

      for (let i = 0; i < result.rows.length; i++) {
        modules.push({
          moduleid: result.rows[i].moduleid,
          moduledescription: result.rows[i].moduledescription,
          modulestatus: result.rows[i].modulestatus
        });

        actions.push({
          actionid: result.rows[i].actionid,
          actiondescription: result.rows[i].actiondescription,
          actionstatus: result.rows[i].actionstatus,
          actionmoduleid: result.rows[i].actionmoduleid
        });
      }

      const filteredModules = await filterArrayNoDuplicates(
        modules,
        "moduleid"
      );

      const filteredActions = await filterArrayNoDuplicates(
        actions,
        "actionid"
      );

      for (let i = 0; i < filteredModules.length; i++) {
        let temp = [];

        for (let i2 = 0; i2 < filteredActions.length; i2++) {
          if (
            filteredModules[i].moduleid === filteredActions[i2].actionmoduleid
          ) {
            temp.push(filteredActions[i2]);
          }
        }

        filteredModules[i].actions = temp;
      }

      return filteredModules;
    }
  };
};

const filterArrayNoDuplicates = (arr, comp) => {
  const unique = arr
    .map(e => e[comp])

    .map((e, i, final) => final.indexOf(e) === i && i)

    .filter(e => arr[e])
    .map(e => arr[e]);

  return unique;
};

module.exports = modulesSelectAllWithActions;
