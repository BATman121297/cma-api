const resetCodeAndPw = ({ users }) => {
  return async function put({ id, ...info }) {
    const mode = info.mode;

    if (mode == 1) {
      delete info.mode;
      delete info.source;

      info.id = id;

      const res = await users.usersUpdate({ info });
      if (res.status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }
      return res;
    } else {
      //   offline mode
    }
  };
};

module.exports = resetCodeAndPw;
