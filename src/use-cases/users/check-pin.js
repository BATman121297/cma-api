const checkPin = ({ users }) => {
  return async function selects(info) {
    const mode = info.mode;

    if (mode == 1) {
      delete info.modules;

      if (isNaN(info.pin)) {
        throw new Error(`Only number allowed.`);
      }

      const exist = await users.selectPinCodeFromId({ info });

      if (exist.data.length === 0) {
        throw new Error(`Invalid pin`);
      }

      return true;
    }
  };
};

module.exports = checkPin;
