const employeesInfoSelectAll = ({ users, validateAccessRights }) => {
  return async function selects(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "admin",
      "view users"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;

      delete info.source;
      delete info.mode;

      const res = await users.employeeInfosSelectAll({ info });

      return res;
    }
  };
};

module.exports = employeesInfoSelectAll;
