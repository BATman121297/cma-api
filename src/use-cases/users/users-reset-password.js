const resetPassword = ({ usersDb, encrypt, insertActivityLogss, users }) => {
  return async function put({ id, ...info }) {
    const mode = info.mode;

    if (mode == 1) {
      delete info.mode;
      delete info.source;

      info.id = id;

      val({ info });

      info.U_TS_PW = info.password;

      delete info.password;
      delete info.repassword;


      if (isNaN(info.U_TS_PIN)) {
        throw new Error(`Pin should be numbers only.`);
      }

      const res = await users.usersUpdate({ info });
      return res;
    } else {
      if (!info.password) {
        throw new Error(`Please enter password`);
      }
      if (!info.repassword) {
        throw new Error(`Please re-enter password`);
      }
      if (info.password !== info.repassword) {
        throw new Error(`Password doesn't match, please try again..`);
      }

      const password = encrypt(info.password);
      await usersDb.resetPasswords({ id, password });

      // logs object
      const logs = {
        action_type: "RESET PASSWORD USER",
        table_affected: "ts_users",
        new_values: null,
        prev_values: null,
        created_at: null,
        updated_at: new Date().toISOString(),
        users_id: id
      };

      await insertActivityLogss({ logs });

      const data = { msg: "Updated password successfully." };
      return data;
    }
  };
};

const val = ({ info }) => {
  const { password, repassword } = info;

  if (!password) {
    const d = {
      msg: "Please enter password."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!repassword) {
    const d = {
      msg: "Please confirm password."
    };
    throw new Error(JSON.stringify(d));
  }

  if (password !== repassword) {
    const d = {
      msg: "Password doesn't match."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = resetPassword;
