const updateUser = ({
  usersDb,
  e_updateUser,
  insertActivityLogss,
  decrypt,
  users,
  objectLowerCaser,
  validateAccessRights
}) => {
  return async function put({ id, ...info }) {
    const mode = info.mode;
    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "admin",
      "edit user"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;

      delete info.mode;
      delete info.source;

      info.id = id;

      info = await objectLowerCaser(info);

      val({ info });

      const check = await users.usersAddSelectById({ info });

      if (check.status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }

      const length = check.data.length;

      if (length > 0) {
        throw new Error(`Employee account already exists.`);
      }

      const check2 = await users.usersAddSelectByEmpId({ info });
      if (check2.status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }
      const length2 = check2.data.length;

      if (length2 > 0) {
        throw new Error(`Employee ID already exists.`);
      }

      const res = await users.usersUpdate({ info });
      return res;
    } else {
      // offline mode
      const entity = e_updateUser(info);

      const exist = await usersDb.selectSingleUser({ id });

      if (exist.rowCount === 0) {
        throw new Error(`User does not exist.`);
      }

      // query previous values
      const previous = await usersDb.getPreviousValuesOfUser({
        id
      });
      const prev_data = previous.rows[0];
      const prev_values = {
        employee_id: decrypt(prev_data.employee_id),
        email: prev_data.email ? decrypt(prev_data.email) : "",
        firstname: decrypt(prev_data.firstname),
        middlename: prev_data.middlename ? decrypt(prev_data.middlename) : "",
        lastname: decrypt(prev_data.lastname),
        name_extension: prev_data.name_extension
          ? decrypt(prev_data.name_extension)
          : "",
        password: decrypt(prev_data.password),
        employee_image: prev_data.employee_image,
        employee_signature: prev_data.employee_signature,
        contact_number: prev_data.contact_number
          ? decrypt(prev_data.contact_number)
          : "",
        role: {
          id: prev_data.role_id,
          name: prev_data.name,
          status: prev_data.status
        }
      };

      // update the user
      const update = await usersDb.updateUser({
        id,
        employee_id: entity.getEmployeeId(),
        email: entity.getEmail(),
        firstname: entity.getFirstName(),
        middlename: entity.getMiddleName(),
        lastname: entity.getLastName(),
        name_extension: entity.getNameExtension(),
        role_id: entity.getRole(),
        employee_image: entity.getImage(),
        employee_signature: entity.getSignature(),
        contact_number: entity.getContactNumber(),
        updated_at: entity.getUpdatedAt()
      });

      const count = update.rowCount; // get the number of inserted data

      const data = {
        msg: `Updated successfully ${count} user.`
      };

      // query the role of user for logs
      const ts_role = await usersDb.getRoleOfUser({ id: entity.getRole() });
      const role = ts_role.rows[0];

      // new values
      const employee = {
        employee_id: decrypt(entity.getEmployeeId()),
        email: entity.getEmail() ? decrypt(entity.getEmail()) : "",
        firstname: entity.getFirstName(),
        middlename: entity.getMiddleName()
          ? decrypt(entity.getMiddleName())
          : "",
        lastname: entity.getLastName(),
        name_extension: entity.getNameExtension()
          ? decrypt(entity.getNameExtension())
          : "",
        role_id: entity.getRole(),
        employee_image: entity.getImage(),
        employee_signature: entity.getSignature(),
        contact_number: entity.getContactNumber()
          ? decrypt(entity.getContactNumber())
          : "",
        updated_at: entity.getUpdatedAt(),
        role
      };

      // logs object
      const logs = {
        action_type: "UPDATE USER",
        table_affected: "ts_users",
        new_values: employee,
        prev_values,
        created_at: null,
        updated_at: new Date().toISOString(),
        users_id: info.users_id
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};

const val = ({ info }) => {
  const { U_TS_EMP_ID, U_TS_ROLE_ID } = info;

  if (!U_TS_EMP_ID) {
    const d = {
      msg: "Please enter employee ID."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_ROLE_ID) {
    const d = {
      msg: "Please enter role ID."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = updateUser;
