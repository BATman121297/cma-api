const selectActivityLogs = ({ usersDb, decrypt }) => {
  return async function selects(dateRange) {


    const result = await usersDb.selectAllActivityLogs(dateRange);
    const logs = result.rows;

    let data = []; //declare empty array
    for await (let d of logs) {
      const users = {
        id: d.users_id ? d.users_id : "",
        firstname: d.firstname ? decrypt(d.firstname) : "",
        lastname: d.lastname ? decrypt(d.lastname) : ""
      };

      await data.push({
        id: d.id,
        users,
        action_type: d.action_type,
        table_affected: d.table_affected,
        new_values: d.new_values,
        prev_values: d.prev_values,
        created_at: d.created_at,
        updated_at: d.updated_at
      });
    }

    return data;
  };
};

module.exports = selectActivityLogs;
