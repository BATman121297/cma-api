const rawMaterialSelectOne = ({ rawMaterialDb, decrypt }) => {
  return async function selects({ id }) {
    const result = await rawMaterialDb.selectOneRawMaterials({ id });
    const rawMaterial = result.rows;

    let data = []; //declare empty array

    for await (let i of rawMaterial) {
      // created by
      const created_by = [
        {
          firstname: i.createdfn ? decrypt(i.createdfn) : i.createdfn,
          lastname: i.createdln ? decrypt(i.createdln) : i.createdln,
          created_at: i.created_at
        }
      ];

      // modified by
      const modified_by = [
        {
          firstname: i.updatedfn ? decrypt(i.updatedfn) : i.updatedfn,
          lastname: i.updatedln ? decrypt(i.updatedln) : i.updatedln,
          updated_at: i.updated_at
        }
      ];

      data.push({
        id: i.id,
        description: i.description,
        created_by,
        modified_by
      });
    }

    return data;
  };
};

module.exports = rawMaterialSelectOne;
