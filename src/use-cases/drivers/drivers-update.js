const updateDriver = ({
  driversDb,
  makeDriver,
  validateAccessRights,
  insertActivityLogss,
  decrypt,
  objectLowerCaser,
  drivers
}) => {
  return async function put({ id, ...info } = {}) {

    const mode = info.mode

    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    const allowed = await validateAccessRights(info.modules, 'truck and driver', 'edit driver')


    if (!allowed) {
      throw new Error(`Access denied`)
    }

    if (mode == 1) {

      delete info.modules

      delete info.mode;
      delete info.source;

      info.id = id

      info = await objectLowerCaser(info)

      val({ info })

      const check = await drivers.driversAddSelectByName({ info })

      const length = check.data.length;

      if (length > 0) {
        throw new Error(`Driver name already exists.`);
      }


      const res = await drivers.driversUpdate({ info });
      return res;


    } else {

      const result = makeDriver(info);

      const driverExist = await driversDb.findByNameUpdate({
        firstname: result.getFirstName(),
        lastname: result.getLastName(),
        id
      });

      if (driverExist.rowCount !== 0) {
        throw new Error("The driver already exist.");
      }

      // query previous values
      const previous = await driversDb.selectOneDriver({ id });
      const prev_data = previous.rows[0];
      const prev_values = {
        id: prev_data.id,
        firstname: decrypt(prev_data.firstname),
        middlename: prev_data.middlename ? decrypt(prev_data.middlename) : "",
        lastname: decrypt(prev_data.lastname),
        name_extension: prev_data.name_extension
          ? decrypt(prev_data.name_extension)
          : ""
      };

      // update in db
      const update = await driversDb.updateDriver({
        firstname: result.getFirstName(),
        middlename: result.getMiddleName(),
        lastname: result.getLastName(),
        name_extension: result.getNameExtension(),
        updated_at: result.getUpdatedAt(),
        id
      });

      const count = update.rowCount; // get the number of updated data

      const data = {
        msg: `Updated successfully ${count} driver.`
      };

      // logs

      // new values
      const new_values = {
        firstname: decrypt(result.getFirstName()),
        middlename: result.getMiddleName() ? decrypt(result.getMiddleName()) : "",
        lastname: decrypt(result.getLastName()),
        name_extension: result.getNameExtension()
          ? decrypt(result.getNameExtension())
          : "",
        updated_at: result.getUpdatedAt()
      };

      // logs object
      const logs = {
        action_type: "UPDATE DRIVER",
        table_affected: "ts_drivers",
        new_values,
        prev_values,
        created_at: null,
        updated_at: new Date().toISOString(),
        users_id: info.users_id
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};

const val = ({ info }) => {
  const { U_TS_FN, U_TS_MN, U_TS_LN } = info;

  if (!U_TS_FN) {
    const d = {
      msg: "Please enter first name."
    };
    throw new Error(JSON.stringify(d));
  }

  // if (!U_TS_MN) {
  //   const d = {
  //     msg: "Please enter middle name."
  //   };
  //   throw new Error(JSON.stringify(d));
  // }
  if (!U_TS_LN) {
    const d = {
      msg: "Please enter last name."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = updateDriver;
