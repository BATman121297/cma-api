const addNewDriver = ({
  driversDb,
  makeDriver,
  validateAccessRights,
  insertActivityLogss,
  decrypt,
  drivers,
  objectLowerCaser
}) => {
  return async function posts(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "truck and driver",
      "add driver"
    );

    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;

      delete info.source;
      delete info.mode;

      const max = await drivers.driversGetMaxCode({});
      const maxCode = max[0].maxCode;

      info = await objectLowerCaser(info);

      info.Code = maxCode;
      info.Name = maxCode;

      val({ info });

      const check = await drivers.driversAddSelectByName({ info });

      if (check.status == 401) {
        // session expired
        throw new Error("Session expired, please login again.");
      }
      const length = check.data.length;

      if (length > 0) {
        throw new Error(`Driver name already exists.`);
      }

      const res = await drivers.driversAdd({ info });

      const reqStatus = res.status;
      if (reqStatus == 201) {
        return res;
      } else {
        // throw error from SL
        throw new Error(res.data.error.message.value);
      }
    } else {
      const result = makeDriver(info);

      const driverExist = await driversDb.findByName({
        firstname: result.getFirstName(),
        lastname: result.getLastName()
      });

      if (driverExist.rowCount !== 0) {
        throw new Error("The driver already exist.");
      }

      // insert to db
      const insert = await driversDb.insertNewDriver({
        firstname: result.getFirstName(),
        middlename: result.getMiddleName(),
        lastname: result.getLastName(),
        name_extension: result.getNameExtension(),
        created_at: result.getCreatedAt()
      });

      const count = insert.rowCount; // get the number of inserted data

      const data = {
        msg: `Inserted successfully ${count} driver.`
      };

      // logs
      // new values
      const new_values = {
        firstname: decrypt(result.getFirstName()),
        middlename: result.getMiddleName()
          ? decrypt(result.getMiddleName())
          : "",
        lastname: decrypt(result.getLastName()),
        name_extension: result.getNameExtension()
          ? decrypt(result.getNameExtension())
          : "",
        created_at: result.getCreatedAt()
      };

      const logs = {
        action_type: "CREATE DRIVER",
        table_affected: "ts_drivers",
        new_values,
        prev_values: null,
        created_at: new Date().toISOString(),
        updated_at: null,
        users_id: info.users_id
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};

const val = ({ info }) => {
  const { U_TS_FN, U_TS_MN, U_TS_LN } = info;

  if (!U_TS_FN) {
    const d = {
      msg: "Please enter first name."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_LN) {
    const d = {
      msg: "Please enter last name."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = addNewDriver;
