const selectAllTruckTypes = ({
  truckTypesDb,
  truckTypes,
  validateAccessRights
}) => {
  return async function get(info) {
    const mode = info.mode;

    if (!info.modules) {
      throw new Error(`Access denied`);
    }

    const allowed = await validateAccessRights(
      info.modules,
      "truck and driver",
      "view truck types"
    );
    
    if (!allowed) {
      throw new Error(`Access denied`);
    }

    if (mode == 1) {
      delete info.modules;

      delete info.source;
      delete info.mode;

      const res = await truckTypes.truckTypesSelectAll({ info });

      return res;
    } else {
      const select = await truckTypesDb.selectAllTruckTypes();

      const view = select.rows;

      return view;
    }
  };
};

module.exports = selectAllTruckTypes;
