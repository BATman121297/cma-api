const addTruckType = ({
  truckTypesDb,
  e_addTruckType,
  insertActivityLogss,
  truckTypes,
  objectLowerCaser,
  validateAccessRights
}) => {
  return async function post(info) {


    const mode = info.mode




    if (!info.modules) {
      throw new Error(`Access denied`)
    }

    const allowed = await validateAccessRights(info.modules, 'truck and driver', 'add truck type')


    if (!allowed) {
      throw new Error(`Access denied`)
    }




    if (mode == 1) {

      delete info.modules

      delete info.modules

      delete info.source;
      delete info.mode;



      const max = await truckTypes.truckTypesGetMaxCode({})
      const maxCode = max[0].maxCode;



      info = await objectLowerCaser(info)

      val({ info })


      info.Code = maxCode;
      info.Name = maxCode;


      const check = await truckTypes.truckTypesAddSelectByName({ info })

      const length = check.data.length;

      if (length > 0) {
        throw new Error(`Truck model already exists.`);
      }



      const res = await truckTypes.truckTypesAdd({ info });

      const reqStatus = res.status;
      if (reqStatus == 201) {
        return res;
      } else {
        // throw error from SL
        throw new Error(res.data.error.message.value);
      }



    } else {


      const entity = await e_addTruckType(info);

      const trucktype = await truckTypesDb.selectTruckTypeAndModel({
        truck_type: entity.getTruckType(),
        truck_model: entity.getTruckModel()
      });

      if (trucktype.rowCount !== 0) {
        throw new Error("The truck type and model already exist.");
      }

      const insert = await truckTypesDb.insertTruckType({
        truck_type: entity.getTruckType(),
        truck_model: entity.getTruckModel(),
        truck_size: entity.getTruckSize(),
        created_at: entity.getCreatedAt()
      });

      const count = insert.rowCount;

      const data = {
        msg: `Inserted successfully ${count} truck type.`
      };

      // logs
      // new values
      const new_values = {
        truck_type: entity.getTruckType(),
        truck_model: entity.getTruckModel(),
        truck_size: entity.getTruckSize(),
        created_at: entity.getCreatedAt()
      };

      const logs = {
        action_type: "CREATE TRUCK TYPE",
        table_affected: "ts_truck_types",
        new_values,
        prev_values: null,
        created_at: new Date().toISOString(),
        updated_at: null,
        users_id: info.users_id
      };

      await insertActivityLogss({ logs });

      return data;
    }
  };
};


const val = ({ info }) => {
  const { U_TS_TRTYPE, U_TS_TRMODEL, U_TS_TRSIZE } = info;

  if (!U_TS_TRTYPE) {
    const d = {
      msg: "Please enter truck type."
    };
    throw new Error(JSON.stringify(d));
  }

  if (!U_TS_TRMODEL) {
    const d = {
      msg: "Please enter truck model."
    };
    throw new Error(JSON.stringify(d));
  }
  if (!U_TS_TRSIZE) {
    const d = {
      msg: "Please enter truck size."
    };
    throw new Error(JSON.stringify(d));
  }
};

module.exports = addTruckType;
