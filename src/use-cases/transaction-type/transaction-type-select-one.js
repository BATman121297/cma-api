const transactionTypeSelectOne = ({ transactionTypeDb }) => {
  return async function selects({ id }) {
    const result = await transactionTypeDb.selectOneTransactionType({ id });
    const ttype = result.rows;

    return ttype;
  };
};

module.exports = transactionTypeSelectOne;
