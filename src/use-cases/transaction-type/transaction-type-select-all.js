const transactionTypeSelectAll = ({ transactionTypeDb, transactionTypes }) => {
  return async function selects(info) {

    const mode = info.mode

    if(mode == 1){

      delete info.source;
      delete info.mode;


     
      const res = await transactionTypes.transactionTypesSelectAll({info});

      return res
    }else{


    const result = await transactionTypeDb.selectAllTransactionType();
    const ttype = result.rows;

    return ttype;
    }
  };
};

module.exports = transactionTypeSelectAll;
