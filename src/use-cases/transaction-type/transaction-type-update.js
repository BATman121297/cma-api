const updateTransactionType = ({
  transactionTypeDb,
  makeTransactionType,
  insertActivityLogss
}) => {
  return async function put({ id, ...info } = {}) {
    const result = makeTransactionType(info);

    const transactionTypeExist = await transactionTypeDb.selectByNameUpdate({
      name: result.getTransactionType(),
      id
    });

    if (transactionTypeExist.rowCount !== 0) {
      throw new Error("The transaction type already exist.");
    }

    // query previous values
    const previous = await transactionTypeDb.selectOneTransactionType({ id });
    const prev_data = previous.rows[0];
    const prev_values = {
      id: prev_data.id,
      transaction_type_name: prev_data.transaction_type_name,
      created_at: prev_data.created_at,
      updated_at: prev_data.updated_at
    };

    // update to db
    const update = await transactionTypeDb.updateTransactionType({
      name: result.getTransactionType(),
      updated_at: result.getUpdatedAt(),
      id
    });

    const count = update.rowCount; // get the number of updated data

    const data = {
      msg: `Updated successfully ${count} transaction type.`
    };

    // logs
    // new values
    const new_values = {
      name: result.getTransactionType(),
      updated_at: result.getUpdatedAt()
    };

    const logs = {
      action_type: "UPDATE TRANSACTION TYPE",
      table_affected: "ts_transaction_types",
      new_values,
      prev_values,
      created_at: null,
      updated_at: new Date().toISOString(),
      users_id: info.users_id
    };

    await insertActivityLogss({ logs });

    return data;
  };
};

module.exports = updateTransactionType;
