const db = ({ dbs }) => {
  return Object.freeze({
    insertNewTruckScale,
    selectTruckScaleName,
    deleteExistingData,
    selectAllTruckScale,
    selectOneTruckScale,
    updateTruckScale,
    selectTruckScaleNameUpdate
  });
  // add new driver
  async function insertNewTruckScale({ ...info }) {
    const db = await dbs();
    const sql =
      "INSERT INTO ts_truckscales (truckscale_name,truckscale_location,truckscale_length,ip_address,device_name,created_at, " +
      "baudrate,parity,databits,stopbits) VALUES ($1,$2,$3,$4,$5,$10,$6,$7,$8,$9);";
    const params = [
      info.truckscale_name,
      info.truckscale_location,
      info.truckscale_length,
      info.ip_address,
      info.device_name,
      info.baudrate,
      info.parity,
      info.databits,
      info.stopbits,
      info.created_at
    ];
    return db.query(sql, params);
  }
  // select by truckscale name to check if exist
  async function selectTruckScaleName({ ...info }) {
    const db = await dbs();
    const sql =
      "SELECT * FROM ts_truckscales WHERE LOWER(truckscale_name) = LOWER($1);";
    const params = [info.truckscale_name];
    return db.query(sql, params);
  }
  // delete data of truckscale
  async function deleteExistingData({ ...info }) {
    const db = await dbs();
    const sql =
      "DELETE FROM ts_truckscales WHERE LOWER(device_name) = LOWER($1) AND ip_address=$2;";
    const params = [info.device_name, info.ip_address];
    return db.query(sql, params);
  }
  // select all truckscale
  async function selectAllTruckScale({}) {
    const db = await dbs();
    const sql = "SELECT * FROM ts_truckscales;";
    return db.query(sql);
  }
  // select one truckscale
  async function selectOneTruckScale({ id }) {
    const db = await dbs();
    const sql = "SELECT * FROM ts_truckscales WHERE id = $1;";
    const params = [id];
    return db.query(sql, params);
  }
  // update truckscale
  async function updateTruckScale({ id, ...info }) {
    const db = await dbs();
    const sql = `UPDATE ts_truckscales SET truckscale_name=$1, truckscale_location=$2, truckscale_length=$3, ip_address=$4,
    device_name=$5, baudrate=$6, parity=$7, databits=$8, stopbits=$9, updated_at=$10 WHERE id=$11;`;
    const params = [
      info.truckscale_name,
      info.truckscale_location,
      info.truckscale_length,
      info.ip_address,
      info.device_name,
      info.baudrate,
      info.parity,
      info.databits,
      info.stopbits,
      info.updated_at,
      id
    ];
    return db.query(sql, params);
  }
  // check if name exist during update
  async function selectTruckScaleNameUpdate({ id, ...info }) {
    const db = await dbs();
    const sql =
      "SELECT * FROM ts_truckscales WHERE LOWER(truckscale_name) = LOWER($1) AND id <> $2;";
    const params = [info.truckscale_name, id];
    return db.query(sql, params);
  }
};

module.exports = db;
