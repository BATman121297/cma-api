const db = ({ dbs }) => {
  return Object.freeze({
    selectOneTruckInfoByPlateNumber, // check during add
    selectOneTruckInfoByPlateNumberUpdate, // check during update
    insertTruckInfo,
    selectAllTruckInfos,
    selectOneTruckInfo,
    updateTruckInfo
  });
  async function selectOneTruckInfoByPlateNumberUpdate({ id, plate_number }) {
    const db = await dbs();
    const sql = `
      SELECT * FROM ts_truck_infos WHERE plate_number = $1 AND id <> $2;
      `;
    const params = [plate_number, id];
    return db.query(sql, params);
  }
  async function selectOneTruckInfoByPlateNumber({ plate_number }) {
    const db = await dbs();
    const sql = `
      SELECT * FROM ts_truck_infos WHERE plate_number = $1
      `;
    const params = [plate_number];
    return db.query(sql, params);
  }

  async function updateTruckInfo({ id, ...info }) {
    const db = await dbs();
    const sql = `
      UPDATE ts_truck_infos SET truck_type_id = $1, plate_number = $2, updated_at = $3
      WHERE id = $4
      `;
    const params = [info.truck_type_id, info.plate_number, info.updated_at, id];
    return db.query(sql, params);
  }

  async function selectOneTruckInfo({ id }) {
    const db = await dbs();
    const sql = `
    SELECT tti.id truckinfo_id,tti.plate_number,ttt.id trucktype_id, ttt.truck_type,ttt.truck_model,ttt.truck_size 
    FROM ts_truck_infos tti LEFT JOIN ts_truck_types ttt
    ON ttt.id = tti.truck_type_id WHERE tti.id =$1;
      `;
    const params = [id];
    return db.query(sql, params);
  }

  async function selectAllTruckInfos() {
    const db = await dbs();
    const sql = `
    SELECT tti.id truckinfo_id,tti.plate_number,ttt.id trucktype_id, ttt.truck_type,ttt.truck_model,ttt.truck_size 
    FROM ts_truck_infos tti LEFT JOIN ts_truck_types ttt
    ON ttt.id = tti.truck_type_id;
      `;
    return db.query(sql);
  }

  async function insertTruckInfo({ ...info }) {
    const db = await dbs();
    const sql = `INSERT INTO ts_truck_infos (truck_type_id,plate_number,created_at)
      VALUES ($1,$2,$3)`;
    const params = [info.truck_type_id, info.plate_number, info.created_at];
    return db.query(sql, params);
  }
};

module.exports = db;
