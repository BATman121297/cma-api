const db = ({ dbs }) => {
  return Object.freeze({
    selectConfigOfDevice
  });
  // select the config of the device; registered by the admin
  async function selectConfigOfDevice({ ...info }) {
    const db = await dbs();
    const sql = `SELECT baudrate,parity,databits,stopbits FROM ts_truckscales WHERE
    ip_address = $1 AND LOWER(device_name) = LOWER($2);`;
    const params = [info.ip_address, info.device_name];
    return db.query(sql, params);
  }
};

module.exports = db;
