const { makeDb } = require("../app");
const db = require("./query");

const modulesDb = makeDb({ db });

module.exports = modulesDb;
