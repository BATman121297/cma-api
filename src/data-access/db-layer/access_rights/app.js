const { makeDb } = require("../app");
const db = require("./query");

const accessRightsDb = makeDb({ db });

module.exports = accessRightsDb;
