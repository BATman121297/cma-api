const https = require("https");
const axios = require("axios");
const dotenv = require("dotenv");

const { hdbClient } = require("../../hdb/app");

dotenv.config();
require("tls").DEFAULT_MIN_VERSION = "TLSv1";

// ############

// base url for sap
const url = process.env.SAP_URL;

// base url for xsjs
const xsjs = process.env.XSJS_URL;

const roles = {
  rolesUpdate: async ({ info }) => {
    try {
      const { cookie, id } = info; // store cookie
      delete info.cookie; // remove cookie
      delete info.id; // remove id
      const res = await axios({
        method: "PATCH",
        url: `${url}/U_BFI_TS_ROLE('${id}')`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });
      const req = {
        status: res.status,
        msg: res.statusText
      };

      info.cookie = cookie
      info.id = id
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  rolesSelectOne: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "GET",
        url: `${url}/$crossjoin(U_BFI_TS_ROLE,U_BFI_TS_USERS,EmployeesInfo)?$expand=U_BFI_TS_ROLE($select=Code, U_TS_NAME, U_TS_STATUS),U_BFI_TS_USERS($select=Code),EmployeesInfo($select=LastName,FirstName)&$filter=U_BFI_TS_ROLE/U_TS_CREATEDBY eq U_BFI_TS_USERS/Code and U_BFI_TS_ROLE/Code eq '${info.id}' and U_BFI_TS_USERS/U_APP_EMP_ID eq EmployeesInfo/EmployeeID`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText,
        data: res.data.value
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  rolesAdd: async ({ info }) => {
    try {
      const cookie = info.cookie; // store cookie
      delete info.cookie; // remove cookie

      const res = await axios({
        method: "POST",
        url: `${url}/U_BFI_TS_ROLE`,
        data: {
          ...info
        },
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      const req = {
        status: res.status,
        msg: res.statusText
      };
      return req;
    } catch (e) {
      const err = {
        status: e.response.status,
        msg: e.response.statusText,
        data: e.response.data
      };
      return err;
    }
  },

  rolesAddSelectByName: async ({ info }) => {

    try {
      const cookie = info.cookie; // store cookie

      const res = await axios({
        method: "GET",
        url: `${url}/U_BFI_TS_ROLE?$filter=U_TS_NAME eq '${info.U_TS_NAME}' and Code ne '${info.id}'`,
        headers: {
          "Content-Type": "application/json",
          Cookie: `${cookie};`
        },
        httpsAgent: new https.Agent({ rejectUnauthorized: false })
      });

      return res.data.value;
    } catch (e) {
      console.log("Error: ", e);
    }
  },


  rolesGetMaxCode: async ({ }) => {
    try {
      const client = await hdbClient();

      const db = process.env.DB;

      const sql = `SELECT COALESCE(MAX(CAST("Code" AS INTEGER)) + 1,1)
      AS "maxCode" FROM "${db}"."@BFI_TS_ROLE";`;

      const result = await new Promise(resolve => {
        client.connect(function (err) {
          if (err) {
            console.log("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });
      return result;
    } catch (e) {
      console.log("Error: ", e);
    }
  },
  rolesSelectAll: async ({ info }) => {
    try {
      const client = await hdbClient();

      const sql = `select "a"."Code", "a"."U_TS_NAME", "a"."U_TS_STATUS", "a"."U_TS_CREATEDATE", "a"."U_TS_CREATETIME",
      "a"."U_TS_UPDATEDATE", "a"."U_TS_UPDATETIME", "c"."firstName", "c"."lastName"
       from "${process.env.DB}"."@BFI_TS_ROLE" "a"
      left join "${process.env.DB}"."@BFI_TS_USERS" "b" on "a"."U_TS_CREATEDBY" = "b"."Code"
      left join "${process.env.reviveDB}"."OHEM" "c" on "b"."U_APP_EMP_ID" = "c"."empID"`;

      const result = await new Promise(resolve => {
        client.connect(function (err) {
          if (err) {
            return console.error("Connect error", err);
          }
          client.exec(sql, function (err, rows) {
            resolve(rows);
            client.end();
          });
        });
      });

      return result;

    } catch (e) {
      console.log("Error: ", e);
    }
  },

  // rolesSelectAll: async ({ info }) => {
  //     try {
  //       const cookie = info.cookie; // store cookie
  //       delete info.cookie; // remove cookie

  //       let data = [];


  //       const request = async link => {
  //         let res;

  //         res = await axios({
  //           method: "GET",
  //           url: `${url}/${link}`,
  //           headers: {
  //             "Content-Type": "application/json",
  //             Cookie: `${cookie};`
  //           },
  //           httpsAgent: new https.Agent({ rejectUnauthorized: false })
  //         });

  //         const arr = res.data.value;
  //         for await (let i of arr) {
  //           data.push(i);
  //         }

  //         // loop always if there is next link
  //         while (res.data["odata.nextLink"]) {
  //           const nextPage = res.data["odata.nextLink"];
  //           await request(nextPage);
  //           break;
  //         }
  //       };

  //       await request(
  //         `$crossjoin(U_BFI_TS_ROLE,U_BFI_TS_USERS,EmployeesInfo)?$expand=U_BFI_TS_ROLE($select=Code, U_TS_NAME, U_TS_STATUS,U_TS_CREATEDATE,U_TS_CREATETIME,U_TS_UPDATEDATE,U_TS_UPDATETIME),U_BFI_TS_USERS($select=Code),EmployeesInfo($select=LastName,FirstName)&$filter=U_BFI_TS_ROLE/U_TS_CREATEDBY eq U_BFI_TS_USERS/Code and U_BFI_TS_USERS/U_APP_EMP_ID eq EmployeesInfo/EmployeeID`
  //       );


  //       return data;

  //     } catch (e) {
  //       const err = {
  //         status: e.response.status,
  //         msg: e.response.statusText,
  //         data: e.response.data
  //       };
  //       return err;
  //     }
  //   }


};

module.exports = { roles };
