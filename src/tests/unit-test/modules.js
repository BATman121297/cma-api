const {
  addNewModules,
  updateModuless,
  modulesSelectAlls,
  modulesSelectOnes,
  uc_modulesSelectAllWithActions
} = require("../../use-cases/modules/app");
const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  selectAllModules: async () => {
    let bool = false;
    let res;
    try {
      res = await modulesSelectAlls({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectOneModule: async ({ id }) => {
    let bool = false;
    let res;
    try {
      res = await modulesSelectOnes(id);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  insertNewModule: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await addNewModules(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  updateModule: async ({ id, ...info }) => {
    let bool = false;
    let res;
    try {
      res = await updateModuless({ id, ...info });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectModuleWithActions: async () => {
    let bool = false;
    let res;
    try {
      res = await uc_modulesSelectAllWithActions({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
