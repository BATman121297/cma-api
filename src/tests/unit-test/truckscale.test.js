const { app, randomString } = require("./truckscale");
const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};

beforeAll(async () => {
  await database();
});



// mock random length; return an integer function
const getRandomInt = max => {
  return Math.floor(Math.random() * Math.floor(max));
};

// functions
describe("Truck scale use cases functions.", () => {
  test("INSERT truck scale.", async () => {
    const info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(10),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    const res = await app.insertTruckScale({ info });
    expect(res.bool).toBe(true);
  });

  test("SELECT all truck scale.", async () => {
    const res = await app.selectAllTruckScale({});
    expect(res.bool).toBe(true);
  });

  test("SELECT one truck scale.", async () => {
    const res = await app.selectAllTruckScale({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random truck scale id

    const data = await app.selectOneTruckScale(id);
    expect(data.res.length).not.toBe(0);
  });

  test("UPDATE truck scale.", async () => {
    // insert
    const ts_info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    await app.insertTruckScale({ info: ts_info });

    const res = await app.selectAllTruckScale({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random truck scale id

    const info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      ip_address: "192.168.0.1",
      device_name: randomString(7),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    const data = await app.updateTruckScale({ id, ...info });
    expect(data.bool).toBe(true);
  });

  test("INSERT truck scale - missing required fields.", async () => {
    const info = {
      truckscale_name: "",
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    const res = await app.insertTruckScale({ info });
    expect(res.bool).toBe(false);
  });

  test("UPDATE truck scale - missing required fields.", async () => {
    const res = await app.selectAllTruckScale({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random truck scale id

    const info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      ip_address: "",
      device_name: randomString(7),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    const data = await app.updateTruckScale({ id, ...info });
    expect(data.bool).toBe(false);
  });
});
