const {
  addNewRawMaterials,
  rawMaterialSelectAlls,
  rawMaterialSelectOnes,
  updateRawMaterials
} = require("../../use-cases/raw-material/app");
const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  selectAllRawMaterial: async () => {
    let bool = false;
    let res;
    try {
      res = await rawMaterialSelectAlls({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectOneRawMaterial: async ({ id }) => {
    let bool = false;
    let res;
    try {
      res = await rawMaterialSelectOnes({ id });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  insertRawMaterial: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await addNewRawMaterials(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  updateRawMaterial: async ({ id, ...info }) => {
    let bool = false;
    let res;
    try {
      res = await updateRawMaterials({ id, ...info });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
