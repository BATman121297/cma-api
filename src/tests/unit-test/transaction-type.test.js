const { app, randomString } = require("./transaction-type");
const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};


beforeAll(async () => {
  await database();
});

// functions
describe("Transaction type use cases functions.", () => {
  test("INSERT transaction type.", async () => {
    const info = {
      name: randomString(5),
      users_id: "1"
    };
    const res = await app.insertTtype({ info });
    expect(res.bool).toBe(true);
  });

  test("SELECT all transaction type.", async () => {
    const res = await app.selectAllTtype({});
    expect(res.bool).toBe(true);
  });

  test("SELECT one transaction type.", async () => {
    const res = await app.selectAllTtype({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random transaction type id
    const id = rand.id;
    const data = await app.selectOneTtype({ id });
    expect(data.res.length).not.toBe(0);
  });

  test("UPDATE one transaction type.", async () => {
    const res = await app.selectAllTtype({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random transaction type id
    const id = rand.id;

    const info = {
      name: randomString(5),
      users_id: "1"
    };

    const data = await app.updateTtype({ id, ...info });
    expect(data.bool).toBe(true);
  });

  test("SELECT one transaction type - no id.", async () => {
    const res = await app.selectAllTtype({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random transaction type id
    const id = rand.id;
    const data = await app.selectOneTtype({});
    expect(data.res.length).toBe(0);
  });

  test("INSERT transaction type - missing required fields.", async () => {
    const info = {
      name: "",
      users_id: "1"
    };
    const res = await app.insertTtype({ info });
    expect(res.bool).toBe(false);
  });

  test("UPDATE one transaction type - missing required fields.", async () => {
    const res = await app.selectAllTtype({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    // random transaction type id
    const id = rand.id;

    const info = {
      name: "",
      users_id: "1"
    };

    const data = await app.updateTtype({ id, ...info });
    expect(data.bool).toBe(false);
  });
});
