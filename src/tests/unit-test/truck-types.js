const {
  uc_addTruckType,
  uc_selectAllTruckTypes,
  uc_selectOneTruckType,
  uc_updateTruckType
} = require("../../use-cases/truck-types/app");
const rs = require("randomstring"); // generate random string

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

const app = {
  selectAllTruckTypes: async () => {
    let bool = false;
    let res;
    try {
      res = await uc_selectAllTruckTypes({});
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  selectOneTruckType: async id => {
    let bool = false;
    let res;
    try {
      res = await uc_selectOneTruckType({ id });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  insertTruckType: async ({ info }) => {
    let bool = false;
    let res;
    try {
      res = await uc_addTruckType(info);
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  },
  updateTruckType: async ({ id, ...info }) => {
    let bool = false;
    let res;
    try {
      res = await uc_updateTruckType({ id, ...info });
      bool = true;
    } catch (e) {
      // console.log("Error: ", e);
    }
    const data = { bool, res };
    return data;
  }
};

module.exports = { app, randomString };
