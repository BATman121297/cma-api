const { app, randomString } = require("./truck-types");
const { db } = require("../../db/app");
const dotenv = require("dotenv");
dotenv.config();
const { uc_seedDefault } = require("../../use-cases/seeders/app");
const database = async () => {
  try {
    process.env.PGDATABASE = await db();
    await uc_seedDefault();
  } catch (e) {
    console.log(e);
  }
};

beforeAll(async () => {
  await database();
});


// functions
describe("Truck types use cases functions.", () => {
  test("INSERT one truck type.", async () => {
    const info = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: 4.5,
      users_id: "1"
    };

    const res = await app.insertTruckType({ info });
    expect(res.bool).toBe(true);
  });

  test("SELECT all truck types.", async () => {
    const res = await app.selectAllTruckTypes({});
    expect(res.bool).toBe(true);
  });

  test("SELECT one truck type.", async () => {
    const res = await app.selectAllTruckTypes({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random truck type id

    const data = await app.selectOneTruckType(id);
    expect(data.res.length).not.toBe(0);
  });

  test("UPDATE one truck type.", async () => {
    const res = await app.selectAllTruckTypes({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random truck type id

    const info = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: 4.5,
      users_id: "1"
    };

    const data = await app.updateTruckType({ id, ...info });
    expect(data.bool).toBe(true);
  });

  test("SELECT one truck type - no id.", async () => {
    const res = await app.selectAllTruckTypes({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random truck type id

    const data = await app.selectOneTruckType();
    expect(data.res.length).toBe(0);
  });

  test("INSERT one truck type - missing required fields.", async () => {
    const info = {
      truck_type: "",
      truck_model: randomString(4),
      truck_size: 4.5,
      users_id: "1"
    };

    const res = await app.insertTruckType({ info });
    expect(res.bool).toBe(false);
  });

  test("UPDATE one truck type - missing required fields.", async () => {
    const res = await app.selectAllTruckTypes({});
    const rand = res.res[Math.floor(Math.random() * res.res.length)];
    const id = rand.id; // random truck type id

    const info = {
      truck_type: "",
      truck_model: randomString(4),
      truck_size: 4.5,
      users_id: "1"
    };

    const data = await app.updateTruckType({ id, ...info });
    expect(data.bool).toBe(false);
  });
});
