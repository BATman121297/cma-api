const { transactions, randomString, getRandomInt } = require("./transactions");
const { users } = require("./users");
const { seeds } = require("./seeders");
const { truckTypes } = require("./truck-types");
const { truckInfo } = require("./truck-info");
const { drivers } = require("./drivers");
const { truckscale } = require("./truckscale");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("Transactions routes.", () => {
  it("POST - Insert new delivery transactions.", async () => {
    // insert truck type
    const info = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: getRandomInt(10),
      users_id: "1"
    };

    await truckTypes.insertTruckType({ token, info });

    // select one truck type; then get id
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    // insert plate number
    const plateNumber = randomString(5);

    const infos = {
      users_id: "1",
      truck_type_id: id,
      plate_number: plateNumber
    };

    await truckInfo.insertTruckInfo({ token, info: infos });

    // insert truckscale
    const t_info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    await truckscale.insertTruckscale({ token, info: t_info });

    // insert transaction
    const tr_info = {
      supplier_name: randomString(5),
      raw_material_name: randomString(3),
      transaction_type_name: "Delivery",
      created_by: "1",
      tracking_code: randomString(5),
      no_of_bags: getRandomInt(500),
      driver: {
        firstname: randomString(3),
        lastname: randomString(3)
      },
      plate_number: plateNumber,
      users_id: "1",
      truckscale_id: "1"
    };

    const datas = await transactions.insertTransactions({
      token,
      info: tr_info
    });
    expect(datas.status).toBe(201);
  });

  it("POST - Insert new others transactions.", async () => {
    const res = await truckInfo.selectAllTruckInfo({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck info id
    const t_id = rand.truckinfo_id;

    const ress = await drivers.selectAllDrivers({ token });
    const datas = ress.data.view;
    const rands = datas[Math.floor(Math.random() * datas.length)];
    // random drivers id
    const d_id = rands.id;

    const info = {
      supplier_name: randomString(5),
      raw_material_name: randomString(4),
      transaction_type_name: "Others",
      created_by: "1",
      drivers_id: d_id,
      truck_info_id: t_id
    };

    const tr = await transactions.insertTransactions({
      token,
      info
    });
    expect(tr.status).toBe(201);
  });

  it("GET - Select all transactions.", async () => {
    const info = {
      from: new Date(),
      to: new Date()
    };
    const res = await transactions.selectAllTransactions({ token, info });
    expect(res.status).toBe(200);
  });

  it("GET - Fetch SAP purchase order id.", async () => {
    const res = await transactions.fetchPurchaseOrder({ token });
    expect(res.status).toBe(201);
  });

  it("POST - Insert inbound and outbound of transactions.", async () => {
    // select one truck type; then get id
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    // insert plate number
    const plateNumber = randomString(5);

    const infos = {
      users_id: "1",
      truck_type_id: id,
      plate_number: plateNumber
    };

    await truckInfo.insertTruckInfo({ token, info: infos });

    // insert truckscale
    const t_info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    await truckscale.insertTruckscale({ token, info: t_info });

    const ress = await truckscale.selectAllTruckscale({ token });
    const datas = ress.data.view;
    const rands = datas[Math.floor(Math.random() * datas.length)];
    // random truckscale id
    const tsId = rands.id;

    // insert transaction
    const tr_info = {
      supplier_name: randomString(5),
      raw_material_name: randomString(3),
      transaction_type_name: "Delivery",
      created_by: "1",
      tracking_code: randomString(5),
      no_of_bags: getRandomInt(500),
      driver: {
        firstname: randomString(3),
        lastname: randomString(3)
      },
      plate_number: plateNumber,
      users_id: "1",
      truckscale_id: tsId
    };

    const tr = await transactions.insertTransactions({
      token,
      info: tr_info
    });

    // transcation id
    const transactionId = tr.data.posted.transactionId;

    const ib = {
      transaction_id: transactionId,
      ib_weight: getRandomInt(20000)
    };

    const inbound = await transactions.insertInbound({ token, info: ib });
    expect(inbound.status).toBe(201);

    // # RESCAN QR
    await transactions.insertTransactions({
      token,
      info: tr_info
    });

    const ob = {
      transaction_id: transactionId,
      ob_weight: getRandomInt(20000),
      truckscale_id: tsId
    };

    const outbound = await transactions.insertOutbound({ token, info: ob });
    expect(outbound.status).toBe(201);
  });

  it("POST - Insert new delivery transactions: missing required data.", async () => {
    // insert truck type
    const info = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: getRandomInt(10),
      users_id: "1"
    };

    await truckTypes.insertTruckType({ token, info });

    // select one truck type; then get id
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    // insert plate number
    const plateNumber = randomString(5);

    const infos = {
      users_id: "1",
      truck_type_id: id,
      plate_number: plateNumber
    };

    await truckInfo.insertTruckInfo({ token, info: infos });

    // insert truckscale
    const t_info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    await truckscale.insertTruckscale({ token, info: t_info });

    // insert transaction
    const tr_info = {
      supplier_name: randomString(5),
      raw_material_name: randomString(3),
      transaction_type_name: "Delivery",
      created_by: "1",
      tracking_code: randomString(5),
      no_of_bags: getRandomInt(500),
      driver: {
        firstname: randomString(3),
        lastname: randomString(3)
      },
      plate_number: plateNumber,
      users_id: "1",
      truckscale_id: ""
    };

    const datas = await transactions.insertTransactions({
      token,
      info: tr_info
    });
    expect(datas.status).toBe(400);
  });

  it("POST - Insert new delivery transactions: no token.", async () => {
    // insert truck type
    const info = {
      truck_type: randomString(5),
      truck_model: randomString(4),
      truck_size: getRandomInt(10),
      users_id: "1"
    };

    await truckTypes.insertTruckType({ token, info });

    // select one truck type; then get id
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    // insert plate number
    const plateNumber = randomString(5);

    const infos = {
      users_id: "1",
      truck_type_id: id,
      plate_number: plateNumber
    };

    await truckInfo.insertTruckInfo({ token, info: infos });

    // insert truckscale
    const t_info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    await truckscale.insertTruckscale({ token, info: t_info });

    // insert transaction
    const tr_info = {
      supplier_name: randomString(5),
      raw_material_name: randomString(3),
      transaction_type_name: "Delivery",
      created_by: "1",
      tracking_code: randomString(5),
      no_of_bags: getRandomInt(500),
      driver: {
        firstname: randomString(3),
        lastname: randomString(3)
      },
      plate_number: plateNumber,
      users_id: "1",
      truckscale_id: "1"
    };

    const datas = await transactions.insertTransactions({
      info: tr_info
    });
    expect(datas.status).toBe(403);
  });

  it("POST - Insert new others transactions: missing required data.", async () => {
    const res = await truckInfo.selectAllTruckInfo({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck info id
    const t_id = rand.truckinfo_id;

    const ress = await drivers.selectAllDrivers({ token });
    const datas = ress.data.view;
    const rands = datas[Math.floor(Math.random() * datas.length)];
    // random drivers id
    const d_id = rands.id;

    const info = {
      supplier_name: randomString(5),
      raw_material_name: randomString(4),
      transaction_type_name: "Others",
      created_by: "1",
      drivers_id: d_id,
      truck_info_id: ""
    };

    const tr = await transactions.insertTransactions({
      token,
      info
    });
    expect(tr.status).toBe(400);
  });

  it("POST - Insert new others transactions: no token.", async () => {
    const res = await truckInfo.selectAllTruckInfo({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck info id
    const t_id = rand.truckinfo_id;

    const ress = await drivers.selectAllDrivers({ token });
    const datas = ress.data.view;
    const rands = datas[Math.floor(Math.random() * datas.length)];
    // random drivers id
    const d_id = rands.id;

    const info = {
      supplier_name: randomString(5),
      raw_material_name: randomString(4),
      transaction_type_name: "Others",
      created_by: "1",
      drivers_id: d_id,
      truck_info_id: t_id
    };

    const tr = await transactions.insertTransactions({
      info
    });
    expect(tr.status).toBe(403);
  });

  it("GET - Select all transactions: no token.", async () => {
    const info = {
      from: new Date(),
      to: new Date()
    };
    const res = await transactions.selectAllTransactions({ info });
    expect(res.status).toBe(403);
  });

  it("POST - Insert inbound and outbound of transactions: missing required data.", async () => {
    // select one truck type; then get id
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    // insert plate number
    const plateNumber = randomString(5);

    const infos = {
      users_id: "1",
      truck_type_id: id,
      plate_number: plateNumber
    };

    await truckInfo.insertTruckInfo({ token, info: infos });

    // insert truckscale
    const t_info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    await truckscale.insertTruckscale({ token, info: t_info });

    const ress = await truckscale.selectAllTruckscale({ token });
    const datas = ress.data.view;
    const rands = datas[Math.floor(Math.random() * datas.length)];
    // random truckscale id
    const tsId = rands.id;

    // insert transaction
    const tr_info = {
      supplier_name: randomString(5),
      raw_material_name: randomString(3),
      transaction_type_name: "Delivery",
      created_by: "1",
      tracking_code: randomString(5),
      no_of_bags: getRandomInt(500),
      driver: {
        firstname: randomString(3),
        lastname: randomString(3)
      },
      plate_number: plateNumber,
      users_id: "1",
      truckscale_id: tsId
    };

    const tr = await transactions.insertTransactions({
      token,
      info: tr_info
    });

    // transcation id
    const transactionId = tr.data.posted.transactionId;

    const ib = {
      transaction_id: transactionId,
      ib_weight: ""
    };

    const inbound = await transactions.insertInbound({ token, info: ib });
    expect(inbound.status).toBe(400);

    // # RESCAN QR
    await transactions.insertTransactions({
      token,
      info: tr_info
    });

    const ob = {
      transaction_id: transactionId,
      ob_weight: "",
      truckscale_id: tsId
    };

    const outbound = await transactions.insertOutbound({ token, info: ob });
    expect(outbound.status).toBe(400);
  });

  it("POST - Insert inbound and outbound of transactions: no token.", async () => {
    // select one truck type; then get id
    const res = await truckTypes.selectAllTruckTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random truck types id
    const id = rand.id;

    // insert plate number
    const plateNumber = randomString(5);

    const infos = {
      users_id: "1",
      truck_type_id: id,
      plate_number: plateNumber
    };

    await truckInfo.insertTruckInfo({ token, info: infos });

    // insert truckscale
    const t_info = {
      truckscale_name: randomString(5),
      truckscale_location: randomString(10),
      truckscale_length: getRandomInt(20),
      baudrate: "9600",
      parity: "none",
      databits: "8",
      stopbits: "1"
    };

    await truckscale.insertTruckscale({ token, info: t_info });

    const ress = await truckscale.selectAllTruckscale({ token });
    const datas = ress.data.view;
    const rands = datas[Math.floor(Math.random() * datas.length)];
    // random truckscale id
    const tsId = rands.id;

    // insert transaction
    const tr_info = {
      supplier_name: randomString(5),
      raw_material_name: randomString(3),
      transaction_type_name: "Delivery",
      created_by: "1",
      tracking_code: randomString(5),
      no_of_bags: getRandomInt(500),
      driver: {
        firstname: randomString(3),
        lastname: randomString(3)
      },
      plate_number: plateNumber,
      users_id: "1",
      truckscale_id: tsId
    };

    const tr = await transactions.insertTransactions({
      token,
      info: tr_info
    });

    // transcation id
    const transactionId = tr.data.posted.transactionId;

    const ib = {
      transaction_id: transactionId,
      ib_weight: getRandomInt(20000)
    };

    const inbound = await transactions.insertInbound({ info: ib });
    expect(inbound.status).toBe(403);

    // # RESCAN QR
    await transactions.insertTransactions({
      token,
      info: tr_info
    });

    const ob = {
      transaction_id: transactionId,
      ob_weight: getRandomInt(20000),
      truckscale_id: tsId
    };

    const outbound = await transactions.insertOutbound({ info: ob });
    expect(outbound.status).toBe(403);
  });

  it("GET - Fetch notif for transmittal.", async () => {
    const res = await transactions.fetchNotifForTransmittal({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Fetch notif for transmittal: no token.", async () => {
    const res = await transactions.fetchNotifForTransmittal({});
    expect(res.status).toBe(403);
  });
});
