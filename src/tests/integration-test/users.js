const axios = require("axios");
const rs = require("randomstring"); // generate random string
const dotenv = require("dotenv");
dotenv.config();

// function to generate random string
const randomString = i => {
  const str = rs.generate({
    length: i,
    charset: "alphabetic"
  });
  return str;
};

// function generate random int
const getRandomInt = max => {
  return Math.floor(Math.random() * Math.floor(max));
};

const users = {
  loginUser: async ({ info }) => {
    try {
      const res = await axios({
        method: "PUT",
        url: `${process.env.BASE_URL}/users/login`,
        data: {
          ...info
        }
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  selectAllUsers: async ({ token }) => {
    try {
      const res = await axios({
        method: "GET",
        url: `${process.env.BASE_URL}/users/select`,
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  selectOneUser: async ({ id, token }) => {
    try {
      const res = await axios({
        method: "GET",
        url: `${process.env.BASE_URL}/users/select/${id}`,
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  insertUser: async ({ token, info }) => {
    try {
      const res = await axios({
        method: "POST",
        url: `${process.env.BASE_URL}/users/add`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          ...info
        }
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  updateUser: async ({ id, token, info }) => {
    try {
      const res = await axios({
        method: "PUT",
        url: `${process.env.BASE_URL}/users/update/${id}`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          ...info
        }
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  resetPassword: async ({ id, token, info }) => {
    try {
      const res = await axios({
        method: "PUT",
        url: `${process.env.BASE_URL}/users/reset-password/${id}`,
        headers: {
          Authorization: `Bearer ${token}`
        },
        data: {
          ...info
        }
      });
      return res;
    } catch (e) {
      return e.response;
    }
  },
  selectAllActivityLogs: async ({ token }) => {
    try {
      const res = await axios({
        method: "GET",
        url: `${process.env.BASE_URL}/users/select-logs`,
        headers: {
          Authorization: `Bearer ${token}`
        }
      });
      return res;
    } catch (e) {
      return e.response;
    }
  }
};

module.exports = { users, randomString, getRandomInt };
