const { weightType, randomString, getRandomInt } = require("./weight-type");
const { users } = require("./users");
const { seeds } = require("./seeders");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("Weight type routes.", () => {
  it("POST - Insert new weight type.", async () => {
    const info = {
      weight_name: randomString(5),
      users_id: "1"
    };

    const res = await weightType.insertWeightType({ token, info });
    expect(res.status).toBe(201);
  });

  it("GET - Select all weight type.", async () => {
    const res = await weightType.selectAllWeightTypes({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Select one weight type.", async () => {
    const res = await weightType.selectAllWeightTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random weight type id
    const id = rand.id;

    const ress = await weightType.selectOneWeightType({ id, token });
    expect(ress.status).toBe(200);
  });

  it("PUT - Update weight type.", async () => {
    const res = await weightType.selectAllWeightTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random weight type id
    const id = rand.id;

    const info = {
      weight_name: randomString(5),
      users_id: "1"
    };

    const ress = await weightType.updateWeightType({ id, token, info });
    expect(ress.status).toBe(200);
  });

  it("POST - Insert new weight type: missing required fields.", async () => {
    const info = {
      weight_name: "",
      users_id: "1"
    };

    const res = await weightType.insertWeightType({ token, info });
    expect(res.status).toBe(400);
  });

  it("POST - Insert new weight type: no token.", async () => {
    const info = {
      weight_name: randomString(5),
      users_id: "1"
    };

    const res = await weightType.insertWeightType({ info });
    expect(res.status).toBe(403);
  });

  it("GET - Select all weight type: no token.", async () => {
    const res = await weightType.selectAllWeightTypes({});
    expect(res.status).toBe(403);
  });

  it("GET - Select one weight type: no token.", async () => {
    const res = await weightType.selectAllWeightTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random weight type id
    const id = rand.id;

    const ress = await weightType.selectOneWeightType({ id });
    expect(ress.status).toBe(403);
  });

  it("PUT - Update weight type: missing required fields.", async () => {
    const res = await weightType.selectAllWeightTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random weight type id
    const id = rand.id;

    const info = {
      weight_name: "",
      users_id: "1"
    };

    const ress = await weightType.updateWeightType({ id, token, info });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update weight type: no token.", async () => {
    const res = await weightType.selectAllWeightTypes({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random weight type id
    const id = rand.id;

    const info = {
      weight_name: randomString(5),
      users_id: "1"
    };

    const ress = await weightType.updateWeightType({ id, info });
    expect(ress.status).toBe(403);
  });
});
