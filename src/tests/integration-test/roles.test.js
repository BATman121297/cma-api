const { roles, randomString, getRandomInt } = require("./roles");
const { users } = require("./users");
const { seeds } = require("./seeders");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("Roles routes.", () => {
  it("POST - Insert new roles.", async () => {
    const info = {
      name: randomString(5),
      created_by: "1",
      users_id: "1"
    };

    const res = await roles.insertRole({ token, info });
    expect(res.status).toBe(201);
  });

  it("GET - Select all roles.", async () => {
    const res = await roles.selectAllRoles({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Select one roles.", async () => {
    const res = await roles.selectAllRoles({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random role id
    const id = rand.id;

    const ress = await roles.selectOneRole({ id, token });
    expect(ress.status).toBe(200);
  });

  it("PUT - Update roles.", async () => {
    const res = await roles.selectAllRoles({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random role id
    const id = rand.id;

    // all are seeded data
    const info = {
      name: randomString(5),
      status: "active",
      modified_by: "1",
      users_id: "1",
      access_rights: [1, 2, 3]
    };

    const ress = await roles.updateRole({ id, token, info });
    expect(ress.status).toBe(200);
  });

  it("POST - Insert new roles: missing required fields.", async () => {
    const info = {
      name: "",
      created_by: "1",
      users_id: "1"
    };

    const res = await roles.insertRole({ token, info });
    expect(res.status).toBe(400);
  });

  it("POST - Insert new roles: no token.", async () => {
    const info = {
      name: randomString(5),
      created_by: "1",
      users_id: "1"
    };

    const res = await roles.insertRole({ info });
    expect(res.status).toBe(403);
  });

  it("GET - Select all roles: no token.", async () => {
    const res = await roles.selectAllRoles({});
    expect(res.status).toBe(403);
  });

  it("GET - Select one roles: no token.", async () => {
    const res = await roles.selectAllRoles({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random role id
    const id = rand.id;

    const ress = await roles.selectOneRole({ id });
    expect(ress.status).toBe(403);
  });

  it("PUT - Update roles: missing required fields.", async () => {
    const res = await roles.selectAllRoles({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random role id
    const id = rand.id;

    // all are seeded data
    const info = {
      name: "",
      status: "active",
      modified_by: "1",
      users_id: "1",
      access_rights: [1, 2, 3]
    };

    const ress = await roles.updateRole({ id, token, info });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update roles: no token.", async () => {
    const res = await roles.selectAllRoles({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random role id
    const id = rand.id;

    // all are seeded data
    const info = {
      name: randomString(5),
      status: "active",
      modified_by: "1",
      users_id: "1",
      access_rights: [1, 2, 3]
    };

    const ress = await roles.updateRole({ id, info });
    expect(ress.status).toBe(403);
  });
});
