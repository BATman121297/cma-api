const { drivers, randomString, getRandomInt } = require("./drivers");
const { users } = require("./users");
const { seeds } = require("./seeders");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("Drivers routes.", () => {
  it("POST - Insert new driver.", async () => {
    const info = {
      firstname: randomString(5),
      middlename: randomString(5),
      lastname: randomString(5),
      name_extension: randomString(2)
    };

    const res = await drivers.insertDriver({ token, info });
    expect(res.status).toBe(201);
  });

  it("GET - Select all drivers.", async () => {
    const res = await drivers.selectAllDrivers({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Select one driver.", async () => {
    const res = await drivers.selectAllDrivers({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random drivers id
    const id = rand.id;

    const ress = await drivers.selectOneDriver({ id, token });
    expect(ress.status).toBe(200);
  });

  it("PUT - Update drivers.", async () => {
    const res = await drivers.selectAllDrivers({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random drivers id
    const id = rand.id;

    const info = {
      firstname: randomString(5),
      middlename: randomString(5),
      lastname: randomString(5),
      name_extension: randomString(2)
    };

    const ress = await drivers.updateDriver({ id, token, info });
    expect(ress.status).toBe(200);
  });

  it("POST - Insert new driver: missing required fields.", async () => {
    const info = {
      firstname: "",
      middlename: randomString(5),
      lastname: randomString(5),
      name_extension: randomString(2)
    };

    const res = await drivers.insertDriver({ token, info });
    expect(res.status).toBe(400);
  });

  it("POST - Insert new driver: no token.", async () => {
    const info = {
      firstname: randomString(5),
      middlename: randomString(5),
      lastname: randomString(5),
      name_extension: randomString(2)
    };

    const res = await drivers.insertDriver({ info });
    expect(res.status).toBe(403);
  });

  it("GET - Select all drivers: no token.", async () => {
    const res = await drivers.selectAllDrivers({});
    expect(res.status).toBe(403);
  });

  it("GET - Select one driver: no token.", async () => {
    const res = await drivers.selectAllDrivers({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random drivers id
    const id = rand.id;

    const ress = await drivers.selectOneDriver({ id });
    expect(ress.status).toBe(403);
  });

  it("PUT - Update drivers: missing required fields.", async () => {
    const res = await drivers.selectAllDrivers({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random drivers id
    const id = rand.id;

    const info = {
      firstname: "",
      middlename: randomString(5),
      lastname: randomString(5),
      name_extension: randomString(2)
    };

    const ress = await drivers.updateDriver({ id, token, info });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update drivers.", async () => {
    const res = await drivers.selectAllDrivers({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random drivers id
    const id = rand.id;

    const info = {
      firstname: randomString(5),
      middlename: randomString(5),
      lastname: randomString(5),
      name_extension: randomString(2)
    };

    const ress = await drivers.updateDriver({ id, info });
    expect(ress.status).toBe(403);
  });
});
