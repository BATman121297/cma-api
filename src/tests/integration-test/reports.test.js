const { reports, randomString, getRandomInt } = require("./reports");
const { users } = require("./users");
const { seeds } = require("./seeders");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("Reports routes.", () => {
  it("GET - Select all completed reports.", async () => {
    const res = await reports.selectAllTransactionsCompleted({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Select all completed reports: no token.", async () => {
    const res = await reports.selectAllTransactionsCompleted({});
    expect(res.status).toBe(403);
  });
});
