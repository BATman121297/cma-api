const { modules, randomString, getRandomInt } = require("./modules");
const { users } = require("./users");
const { seeds } = require("./seeders");

let token = "";

const prereq = async () => {
  try {
    await seeds.seeders({});

    // seeded data
    const info = {
      employee_id: "154151000",
      password: "password"
    };

    const res = await users.loginUser({ info });
    token = res.data.patched.token;
  } catch (e) {
    console.log("Error: ", e);
  }
};

beforeAll(async () => {
  try {
    await prereq();
  } catch (e) {
    console.log(e);
  }
});

// functions
describe("Modules routes.", () => {
  it("POST - Insert new modules.", async () => {
    const info = {
      description: randomString(10),
      created_by: "1",
      users_id: "1"
    };

    const res = await modules.insertModule({ token, info });
    expect(res.status).toBe(201);
  });

  it("GET - Select all modules.", async () => {
    const res = await modules.selectAllModules({ token });
    expect(res.status).toBe(200);
  });

  it("GET - Select one modules.", async () => {
    const res = await modules.selectAllModules({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random module id
    const id = rand.id;

    const ress = await modules.selectOneModules({ id, token });
    expect(ress.status).toBe(200);
  });

  it("GET - Select all modules with actions.", async () => {
    const res = await modules.selectModulesWithActions({ token });
    expect(res.status).toBe(200);
  });

  it("PUT - Update module.", async () => {
    const res = await modules.selectAllModules({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random module id
    const id = rand.id;

    const info = {
      description: randomString(10),
      status: "active",
      modified_by: "1",
      users_id: "1"
    };

    const ress = await modules.updateModule({ id, token, info });
    expect(ress.status).toBe(200);
  });

  it("GET - Select all modules: no token.", async () => {
    const res = await modules.selectAllModules({});
    expect(res.status).toBe(403);
  });

  it("GET - Select all modules with actions: no token.", async () => {
    const res = await modules.selectModulesWithActions({});
    expect(res.status).toBe(403);
  });

  it("GET - Select one modules: no token.", async () => {
    const res = await modules.selectAllModules({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random module id
    const id = rand.id;

    const ress = await modules.selectOneModules({ id });
    expect(ress.status).toBe(403);
  });

  it("POST - Insert new modules: missing required fields.", async () => {
    const info = {
      description: "",
      created_by: "1",
      users_id: "1"
    };

    const res = await modules.insertModule({ token, info });
    expect(res.status).toBe(400);
  });

  it("POST - Insert new modules: no token.", async () => {
    const info = {
      description: randomString(10),
      created_by: "1",
      users_id: "1"
    };

    const res = await modules.insertModule({ info });
    expect(res.status).toBe(403);
  });

  it("PUT - Update module: missing required fields.", async () => {
    const res = await modules.selectAllModules({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random module id
    const id = rand.id;

    const info = {
      description: "",
      status: "active",
      modified_by: "1",
      users_id: "1"
    };

    const ress = await modules.updateModule({ id, token, info });
    expect(ress.status).toBe(400);
  });

  it("PUT - Update module: no token.", async () => {
    const res = await modules.selectAllModules({ token });
    const data = res.data.view;
    const rand = data[Math.floor(Math.random() * data.length)];
    // random module id
    const id = rand.id;

    const info = {
      description: randomString(10),
      status: "active",
      modified_by: "1",
      users_id: "1"
    };

    const ress = await modules.updateModule({ id, info });
    expect(ress.status).toBe(403);
  });
});
