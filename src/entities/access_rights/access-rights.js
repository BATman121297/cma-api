const makeAccessRightsEntity = ({}) => {
  return function make({
    actions_id,
    roles_id,
    created_at = new Date().toISOString()
  } = {}) {
    if (!actions_id) {
      throw new Error("Please select action.");
    }
    if (!roles_id) {
      throw new Error("Please select role.");
    }
    return Object.freeze({
      getActionsId: () => actions_id,
      getRolesId: () => roles_id,
      getCreatedAt: () => created_at
    });
  };
};

module.exports = makeAccessRightsEntity;
