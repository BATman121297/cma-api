const os = require("os");
const address = require("address");

// return ip address of device
const returnIpAddress = () => {
  return address.ip();
};

// get host name of the device
const hostName = () => {
  return os.hostname();
};

const readingDataFromSerial = require("./read-data");

const readData = readingDataFromSerial({ returnIpAddress, hostName });

module.exports = readData;
