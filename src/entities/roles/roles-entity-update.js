const makeRolesEntityUpdate = ({}) => {
  return function make({
    name,
    status,
    modified_by,
    updated_at = new Date().toISOString()
  } = {}) {
    if (!name) {
      throw new Error("Please enter role name.");
    }
    if (!status) {
      throw new Error("Please enter role status.");
    }
    if (!modified_by) {
      throw new Error("Please enter who modified the role.");
    }
    return Object.freeze({
      getRoleName: () => name,
      getRoleStatus: () => status,
      getModifiedBy: () => modified_by,
      getUpdatedAt: () => updated_at
    });
  };
};

module.exports = makeRolesEntityUpdate;
