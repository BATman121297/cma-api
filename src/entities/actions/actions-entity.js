const makeActionsEntity = ({}) => {
  return function make({
    module_id,
    description,
    created_at = new Date().toISOString(),
    status = "active",
    created_by
  } = {}) {
    if (!module_id) {
      throw new Error("Please select which module the action belongs.");
    }
    if (!description) {
      throw new Error("Please enter action name.");
    }
    // if (!status) {
    //   throw new Error("Please enter status.");
    // }
    if (!created_by) {
      throw new Error("Please enter who created the action.");
    }
    return Object.freeze({
      getModuleId: () => module_id,
      getDesc: () => description,
      getStatus: () => status,
      getCreatedBy: () => created_by,
      getCreatedAt: () => created_at
    });
  };
};

module.exports = makeActionsEntity;
