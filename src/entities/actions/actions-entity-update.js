const makeActionsEntityUpdate = ({}) => {
  return function make({
    module_id,
    description,
    status,
    modified_by,
    updated_at = new Date().toISOString()
  } = {}) {
    if (!module_id) {
      throw new Error("Please select which module the action belongs.");
    }
    if (!description) {
      throw new Error("Please enter action name.");
    }
    if (!status) {
      throw new Error("Please enter status.");
    }
    if (!modified_by) {
      throw new Error("Please enter who modified the action.");
    }
    return Object.freeze({
      getModuleId: () => module_id,
      getDesc: () => description,
      getStatus: () => status,
      getModifiedBy: () => modified_by,
      getUpdatedAt: () => updated_at
    });
  };
};

module.exports = makeActionsEntityUpdate;
