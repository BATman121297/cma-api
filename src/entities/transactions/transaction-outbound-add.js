const addOutbound = ({}) => {
  return function make({
    transaction_id,
    weight_type_id,
    ob_weight,
    ob_timestamp = new Date().toISOString(),
    users_id,
    truckscale_id,
    drivers_id,
    truck_info_id,
    inbound_id, // optional only during delivery (special case)
    remarks
  } = {}) {
    if (!transaction_id) {
      throw new Error("Please enter transaction.");
    }
    if (!ob_weight) {
      throw new Error("Please enter outbound weight.");
    }
    if (!users_id) {
      throw new Error("Please enter user.");
    }
    if (!truckscale_id) {
      throw new Error("Please enter truckscale.");
    }
    if (!drivers_id) {
      throw new Error("Please enter driver.");
    }
    if (!truck_info_id) {
      throw new Error("Please enter truck info.");
    }
    return Object.freeze({
      getTransactionId: () => transaction_id,
      getWeightTypeId: () => weight_type_id,
      getObWeight: () => ob_weight,
      getObTimestamp: () => ob_timestamp,
      getUsersId: () => users_id,
      getTruckscaleId: () => truckscale_id,
      getDriversId: () => drivers_id,
      getTruckInfoId: () => truck_info_id,
      getInboundId: () => inbound_id,
      getRemarks: () => remarks
    });
  };
};

module.exports = addOutbound;
