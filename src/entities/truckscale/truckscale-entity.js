const makeTruckScaleEntity = ({ returnIpAddress, hostName, allNumbers }) => {
  return function make({
    truckscale_name,
    truckscale_location,
    truckscale_length,
    ip_address = returnIpAddress(),
    device_name = hostName(),
    baudrate,
    parity,
    databits,
    stopbits,
    created_at = new Date().toISOString(),
    updated_at = new Date().toISOString()
  } = {}) {
    if (!truckscale_name) {
      throw new Error("Please enter truck scale name.");
    }
    if (!truckscale_location) {
      throw new Error("Please enter truck scale location.");
    }
    if (!truckscale_length) {
      throw new Error("Please enter truck scale length in meters.");
    }
    if (allNumbers(truckscale_length) === false) {
      throw new Error("Only numerics are allowed in length of truck scale.");
    }
    if (!baudrate) {
      throw new Error("Please enter baud rate of the device.");
    }
    if (allNumbers(baudrate) === false) {
      throw new Error("Only numerics are allowed in baud rate.");
    }
    if (!parity) {
      throw new Error("Please enter parity of the device.");
    }
    if (!databits) {
      throw new Error("Please enter data bits of the device.");
    }
    if (allNumbers(databits) === false) {
      throw new Error("Only numerics are allowed in data bits.");
    }
    if (!stopbits) {
      throw new Error("Please enter stop bits of the device.");
    }
    if (allNumbers(stopbits) === false) {
      throw new Error("Only numerics are allowed in stop bits.");
    }
    return Object.freeze({
      getTSName: () => truckscale_name,
      getTSLocation: () => truckscale_location,
      getTSLength: () => truckscale_length,
      getIp: () => ip_address,
      getHostName: () => device_name,
      getBaudRate: () => baudrate,
      getParity: () => parity,
      getDataBits: () => databits,
      getStopBits: () => stopbits,
      getCreatedAt: () => created_at,
      getUpdatedAt: () => updated_at
    });
  };
};

module.exports = makeTruckScaleEntity;
