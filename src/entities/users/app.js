const { encrypt } = require("../../../crypting/app");

// ##############################
const makeUserEntity = require("./users-entity");
const userLoginEntity = require("./users-login-entity");
const updateUser = require("./users-entity-update")

// ##############################
const makeUser = makeUserEntity({ encrypt });
const userLoginEntitys = userLoginEntity({ encrypt });
const e_updateUser = updateUser({encrypt})

module.exports = { makeUser, userLoginEntitys, e_updateUser };
