const makeRawMaterialEntityUpdate = ({}) => {
  return function make({
    description,
    updated_by,
    updated_at = new Date().toISOString()
  } = {}) {
    if (!description) {
      throw new Error("Please enter raw material description.");
    }
    if (!updated_by) {
      throw new Error("Please enter who updated the raw material data.");
    }
    return Object.freeze({
      getDescription: () => description,
      getUpdatedBy: () => updated_by,
      getUpdatedAt: () => updated_at
    });
  };
};

module.exports = makeRawMaterialEntityUpdate;
