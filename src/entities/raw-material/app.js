const makeRawMaterialEntity = require("./raw-material-entity");
const makeRawMaterialEntityUpdate = require("./raw-material-entity-update");
// #############################
const makeRawMaterial = makeRawMaterialEntity({});
const makeRawMaterialUpdate = makeRawMaterialEntityUpdate({});

// #############################

module.exports = { makeRawMaterial, makeRawMaterialUpdate };
